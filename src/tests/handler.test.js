const assert = require('assert') // Built in node library, no need to install
const sinon = require('sinon')
const { DynamoDB } = require('aws-sdk')
const { handler } = require('../lambda-functions/get_user.js')
const chai = require('chai')

// create a sinon sandbox
const sandbox = sinon.createSandbox()

describe('Status code 200 is returned', () => {
	describe('get_user_handler()', () => {

		beforeEach(() => sandbox
			// Here I overwrite every call to #query()
			.stub(DynamoDB.DocumentClient.prototype, 'query')
			// And define my own return
			.returns({
				// promise function is required here because we use the
				// #AWS.request.promise() syntax in the handler (line 24)
				promise: () => (    {
      statusCode: 200,
      body: '{"Items":[{"SName": "yong kheng seo", "UserEmail": "neaven77@gmail.com","SK": "usr-neaven77@gmail.com","CustomInfo": "{\"availability\":{\"weekendEvening\":\"\",\"weekendAfternoon\":\"\",\"weekendMorning\":\"\",\"weekdayEvening\":\"\",\"weekdayAfternoon\":\"\",\"weekdayMorning\":\"\"},\"interests\":{\"economic\":\"\",\"education\":\"\",\"energy\":\"\",\"health\":\"\",\"hunger\":\"\",\"inequality\":\"\",\"poverty\":\"\"},\"skills\":{\"marketing\":\"Beginner\",\"dataAnalysis\":\"Beginner\"},\"phone\":\"94379347\"}"\
      "Status": "active","Description": "Tell us about yourself","PK": "usr-neaven77@gmail.com","UserName": "Yong Kheng Seo","GitLabInfo": "[{\"id\":6499199,\"name\":\"Neaven Seo\",\"username\":\"neaven77\",\"state\":\"active\",\"avatar_url\":\"https://secure.gravatar.com/avatar/d611aa060da4150baf726d83bbf77f02?s=80&d=identicon\",\"web_url\":\"https://gitlab.com/neaven77\"}],"\
      "EntityType": "user","Tenent": "p4gTENANT","Name": "Yong Kheng Seo"}],"Count":0,"ScannedCount":0}',
      headers: { 'Access-Control-Allow-Origin': '*' }
    })
			})
		)

		// Runs after every test.  Undo our temporary
		// overwrite so the next test starts clean
		afterEach(() => sandbox.restore())

		it('gets user from the database', async () => {
      const expected = {
        "Items": [ 
            {
              "SName": "yong kheng seo",
              "UserEmail": "neaven77@gmail.com",
              "SK": "usr-neaven77@gmail.com",
              "CustomInfo": "{\"availability\":{\"weekendEvening\":\"\",\"weekendAfternoon\":\"\",\"weekendMorning\":\"\",\"weekdayEvening\":\"\",\"weekdayAfternoon\":\"\",\"weekdayMorning\":\"\"},\"interests\":{\"economic\":\"\",\"education\":\"\",\"energy\":\"\",\"health\":\"\",\"hunger\":\"\",\"inequality\":\"\",\"poverty\":\"\"},\"skills\":{\"marketing\":\"Beginner\",\"dataAnalysis\":\"Beginner\"},\"phone\":\"94379347\"}",
              "Status": "active",
              "Description": "Tell us about yourself",
              "PK": "usr-neaven77@gmail.com",
              "UserName": "Yong Kheng Seo",
              "GitLabInfo": "[{\"id\":6499199,\"name\":\"Neaven Seo\",\"username\":\"neaven77\",\"state\":\"active\",\"avatar_url\":\"https://secure.gravatar.com/avatar/d611aa060da4150baf726d83bbf77f02?s=80&d=identicon\",\"web_url\":\"https://gitlab.com/neaven77\"}]",
              "EntityType": "user",
              "Tenent": "p4gTENANT",
              "Name": "Yong Kheng Seo"
            }
          ],
          "Count": 1,
          "ScannedCount": 1
        }
			const response = await handler({ pathParameters: { userid: "neaven77@gmail.com" } })
      console.log(response)
      assert.equal(response.statusCode, 200)
		})
	})
})