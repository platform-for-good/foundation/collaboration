const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient({region: 'us-east-1'});

exports.handler = async (event, context, callback) => {
	
	var operation = event.operation;

	switch (operation) {
			case 'createUser':
				console.log(event.payload.Item);
				console.log(event.payload.Item.Name);	
				await createUser(event).then(() => {
					callback(null, {
						statusCode: 201,
						body: 'createdUser',
						headers: {
							'Access-Control-Allow-Origin' : '*'
						}
					});
				}).catch((err) => {
					console.error(err);
				});				
				break;

			case 'updateUser':	
				console.log(event.payload.Item);
				console.log(event.payload.Item.Name);			
				break;

			case 'deleteUser':			
				console.log(event.payload.Item);
				console.log(event.payload.Item.Name);			
				break;

			case 'getUser':			
				console.log(event.payload.Item);
				console.log(event.payload.Item.Name);			
				break;	
				
			case 'listAllUsers':
				console.log(event.payload.Item);
				console.log(event.payload.Item.Name);	
				await listAllUsers(event).then(() => {
					callback(null, {
						statusCode: 200,
						body: 'Retreived All Users',
						headers: {
							'Access-Control-Allow-Origin' : '*'
						}
					});
				}).catch((err) => {
					console.error(err);
				});				
				break;

			case 'listUsersByName':			
				console.log(event.payload.Item);
				console.log(event.payload.Item.Name);			
				break;
				
			default:
				callback('Unknown operation: ${operation}');
		}
	

};

function createUser(event) {
	const uniqueId = 'usr-'+(new Date()).getTime().toString(36) + Math.random().toString(36).slice(2);
	//let requestBody = JSON.parse(event);
    const params = {
        TableName: 'platform4good',
        Item: {
            'PK' : uniqueId,
			'SK' : uniqueId,
            'EntityType' : 'User',
	        'Name' :event.payload.Item.Name,
        	'Status' : 'active',
        	'UsrName' : event.payload.Item.UsrName,
        	'UsrEmail' : event.payload.Item.UsrEmail,
        	'SName' : event.payload.Item.SName
        }
    };
    return ddb.put(params).promise();
}

function listAllUsers(event) {
    const params = {
        TableName: 'platform4good',
		IndexName: 'EntityByType',
        KeyConditionExpression: '#EntityType = :value',
		ExpressionAttributeNames: {
			"#EntityType": "EntityType"
		},		
		ExpressionAttributeValues: {
                ':value': 'User'
            }		
    };                                                                                                                                                                                                                                                                                                     
    return ddb.query(params).promise();
}