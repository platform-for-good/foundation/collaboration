const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient({region: 'us-east-1'});
var lambda = new AWS.Lambda({region: 'us-east-1'});

exports.handler = async (event,context) => {

    var user = JSON.parse(event.body);
    var userId = user.userid;
	userId = "usr-"+userId;
    var userName = user.name; 
    var userNameLowerCase = userName.toLowerCase();
    var userDescription = user.description;
    var userCustomInfo = user.custominfo;
    var userGitLabUserName = user.gitlabusername;
    var response = {};
	//retreive user to validate if the user exists
	const userToBeUpdated = await ddb.get({
              TableName: "platform4good-collaboration",
              Key: { "PK": userId,
                     "SK": userId }
            }).promise();
    if (!Object.keys(userToBeUpdated).length){
        response = {
            statusCode: 500,
            body: JSON.stringify("Failed to Update, User doesnot exist"),
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        }
        return response;
    }
   //if userGitLabUserName is provided, retreive userGitLabUserName to validate userGitLabUserName 
   if (userGitLabUserName > ""){
       
        //call sharedservice lambda to validate GitLabUserName 
        const gitLabUserNameToValidate = {
              "operation": "users.get",
              "gitlabusername": userGitLabUserName
            };
        const validatedGitLabUser = await lambda.invoke({ 
                                            FunctionName: 'p4g-tcoe-sharedservice-gitlabservice', 
                                            Payload: JSON.stringify(gitLabUserNameToValidate)
                                        }).promise();
        const validatedGitLabUserPayload= JSON.parse(validatedGitLabUser.Payload);
        console.log(validatedGitLabUser);
        
       if (!Object.keys(validatedGitLabUserPayload).length){
            response = {
                statusCode: 500,
                body: JSON.stringify("Failed to Update, GitLabUserName is not valid"),
                headers: {
                    'Access-Control-Allow-Origin': '*'
                }
            }
            return response;
        } else {
               //update the user gitlabinfo
                var collaborationUserToUpdate = {
                    TableName: "platform4good-collaboration",
                    Key: {
                        'PK' : userId,
            			'SK' : userId
                    },
                    UpdateExpression: "set GitLabInfo = :gitlabinfo",
                    ExpressionAttributeValues: {
                        ":gitlabinfo": validatedGitLabUser.Payload
                    }
                };
            	await ddb.update(collaborationUserToUpdate).promise();
	
            
        }
        
   }
    
   //update other fields of the user
        collaborationUserToUpdate = {
        TableName: "platform4good-collaboration",
        Key: {
            'PK' : userId,
			'SK' : userId
        },
        UpdateExpression: "set #Nam = :userName, UserName = :userName, SName = :userNameLowerCase, Description = :userDescription, CustomInfo = :userCustomInfo",
        ExpressionAttributeValues: {
            ":userName": userName,
            ":userNameLowerCase":userNameLowerCase,
            ":userDescription": userDescription,
            ":userCustomInfo": userCustomInfo
        },
        ExpressionAttributeNames: {
         '#Nam': "Name"
        }
    };
	await ddb.update(collaborationUserToUpdate).promise();
	
    response = {
        statusCode: 200,
        body: JSON.stringify("User was updated successfully"),
        headers: {
            'Access-Control-Allow-Origin': '*'
        }
    };
    
    return response; 
};
