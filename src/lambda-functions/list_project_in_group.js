const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient({region: 'us-east-1'});

exports.handler = async (event, context, callback) => {
    console.log(`List projects in group function invoked with payload: ${event.payload.Item}`)	
};