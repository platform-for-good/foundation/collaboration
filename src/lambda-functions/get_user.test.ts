import { APIGatewayProxyEvent } from 'aws-lambda';
import { expect } from 'chai';
import * as nock from 'nock';
import { handler } from '../lambda-functions/get_user';
 
process.env.dynamotable = 'platform4good-collaboration';
 
describe('getUsers', () => {
 
    it('UT001 - getUsers with valid response', async() => {
         
        nock.recorder.rec();
         
        const event: APIGatewayProxyEvent = {
            body: '',
            headers: {},
            httpMethod: 'GET',
            isBase64Encoded: false,
            path: '',
            pathParameters: {},
            queryStringParameters: undefined,
            stageVariables: {},
            requestContext: {},
            resource: '' };
 
        const response = await handler(event);
        expect(response.statusCode).to.equal(200);
    });
 
});