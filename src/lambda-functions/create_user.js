const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient({ region: 'us-east-1' });

exports.handler = async (event, context) => {
    var user = JSON.parse(event.body);
    var userId = user.userid;
    userId = "usr-" + userId;
    var userName = user.name;
    var userEmail = user.email;
    var userDescription = user.description;
    var tenent = user.tenent;

    var response = {};
    //retreive user to validate if the user already exists
    const userToBeCreated = await ddb.get({
        TableName: "platform4good-collaboration",
        Key: {
            "PK": userId,
            "SK": userId
        }
    }).promise();

    if (Object.keys(userToBeCreated).length) {
        response = {
            statusCode: 500,
            body: JSON.stringify("Failed to Save, User already exist"),
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        }
        return response;
    }

    const collaborationUserToCreate = {
        TableName: 'platform4good-collaboration',
        Item: {
            'PK': userId,
            'SK': userId,
            'EntityType': 'user',
            'Name': userName,
            'UserName': userName,
            'Description': userDescription,
            'UserEmail': userEmail,
            'Status': 'active',
            'Tenent': tenent,
            'SName': userName.toLowerCase()
        }
    };
    await ddb.put(collaborationUserToCreate).promise();
    response = {
        statusCode: 200,
        body: JSON.stringify("User was created successfully"),
        headers: {
            'Access-Control-Allow-Origin': '*'
        }
    };

    return response;
};
