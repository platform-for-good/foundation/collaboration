const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient({ region: 'us-east-1' });
var lambda = new AWS.Lambda({ region: 'us-east-1' });
const uniqueId = 'grp-' + (new Date()).getTime().toString(36);

exports.handler = async (event, context) => {

    var group = JSON.parse(event.body);
    var groupName = group.name;
    var groupDescription = group.description;
    var creatorUserId = 'usr-' + group.userid; //nilesh
    var creatorGitLabUserId = group.gitlabuserid; //6493059

    //1.retreive creatorUserInfo from collaboration
    const creatorUser = await ddb.get({
        TableName: "platform4good-collaboration",
        Key: {
            "PK": creatorUserId,
            "SK": creatorUserId
        }
    }).promise();
    //const creatorUserGitLabInfo = JSON.parse(creatorUser.Item.GitLabInfo);
    //const creatorUserGitLabId = creatorUserGitLabInfo.id;

    //sharedservice lambda 2.create gitlab group 3.added creator as owner
    const gitLabGroupToCreate = {
        "operation": "groups.create",
        "name": groupName,
        "description": groupDescription,
        "gitlabuserid": creatorGitLabUserId //6499199 //creatorUserGitLabId //creatorUser.Item.GitLabInfo.id //6499199
    };

    const createdGitLabGroup = await lambda.invoke({
        FunctionName: 'p4g-tcoe-sharedservice-gitlabservice',
        Payload: JSON.stringify(gitLabGroupToCreate)
    }).promise();
    const createdGitLabGroupPayload = JSON.parse(createdGitLabGroup.Payload);
    const createdGitLabGroupPayloadErrorType = createdGitLabGroupPayload.errorType;

    if (createdGitLabGroupPayloadErrorType > "") {
        var response = {
            statusCode: 500,
            body: JSON.stringify(createdGitLabGroupPayload.errorMessage),
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        };
        return response;
    }
    //4.create group record in collaboration dynamodb.                                   
    const collaborationgGroupToCreate = {
        TableName: 'platform4good-collaboration',
        Item: {
            'PK': uniqueId,
            'SK': uniqueId,
            'EntityType': 'group',
            'Name': groupName,
            'Description': groupDescription,
            'Status': 'active',
            'Type': 'foundation',
            'GroupName': groupName,
            'Tenent': "p4gTENANT",
            'SName': groupName.toLowerCase(),
            'GitLabInfo': createdGitLabGroup.Payload
        }
    };
    await ddb.put(collaborationgGroupToCreate).promise();

    //5.retreive created Collaboration Group
    const createdCollaborationGroup = await ddb.get({
        TableName: "platform4good-collaboration",
        Key: {
            "PK": uniqueId,
            "SK": uniqueId
        }
    }).promise();
    console.log(createdCollaborationGroup);

    //6.Add Creator as group-member with owner role to Collaboration Group
    const collaborationgGroupMemberToAdd = {
        TableName: 'platform4good-collaboration',
        Item: {
            'PK': createdCollaborationGroup.Item.PK,
            'SK': creatorUser.Item.PK,
            'EntityType': 'group-member',
            'GroupName': createdCollaborationGroup.Item.Name,
            'UserName': creatorUser.Item.Name,
            'UserEmail': creatorUser.Item.UserEmail,
            'MemberRole': 40
        }
    };

    await ddb.put(collaborationgGroupMemberToAdd).promise();

    var response = {
        statusCode: 200,
        body: JSON.stringify(createdCollaborationGroup),
        headers: {
            'Access-Control-Allow-Origin': '*'
        }

    };

    return response;
};

