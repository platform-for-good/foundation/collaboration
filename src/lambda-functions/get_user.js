const AWS = require('aws-sdk');
AWS.config.update({region:'us-east-1'});
const ddb = new AWS.DynamoDB.DocumentClient();


exports.handler = async (event, context, callback) => {
    console.log("get_user handler invoked ... ")
    const params = {
        TableName: 'platform4good-collaboration',
        IndexName: 'InvertedKey',
        KeyConditionExpression: '#InvertedKey = :value',
        ExpressionAttributeNames: {
            "#InvertedKey": "SK"
        },
        ExpressionAttributeValues: {
            ':value': event.pathParameters.userid
        }
    };

    const usrs = await ddb.query(params).promise();
    var response = {
        statusCode: 200,
        body: JSON.stringify(usrs),
        headers: {
            'Access-Control-Allow-Origin': '*'
        }

    };

    return response;

};

// function getUser(event) {
//     const params = {
//         TableName: 'platform4good-collaboration',
//         IndexName: 'InvertedKey',
//         KeyConditionExpression: '#InvertedKey = :value',
//         ExpressionAttributeNames: {
//             "#InvertedKey": "SK"
//         },      
//         ExpressionAttributeValues: {
//                 ':value': event.userid
//             }       
//     }; 
//     return ddb.query(params).promise();
// }