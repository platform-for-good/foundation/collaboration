const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient({region: 'us-east-1'}); 

//**************************************************
//**************************************************
//Get All User Info by UserID (SK)
//(including group membership and project membership)
//**************************************************
//**************************************************
exports.handler = async (event, context) => {
 try {
        console.log(`Get all User Info function invoked with userID: ${event.pathParameters.userID}`)
        const params = {
        
        TableName: 'platform4good-collaboration',
        IndexName: 'InvertedKey',
        KeyConditionExpression: '#SK = :value',
		ExpressionAttributeNames: {
			"#SK": "SK"
		},		
		ExpressionAttributeValues: {
                ':value': event.pathParameters.userID
            }	
    };  
        var resultList = await ddb.query(params).promise();
        var response = {
                        statusCode: 200,
                        body: JSON.stringify(resultList),
                        headers: {
                            'Access-Control-Allow-Origin' : '*'
                        }
                    };
		return response;
		
 } catch (error) 
 {
        console.error(error);
 }
};