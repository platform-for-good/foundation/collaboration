const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient({ region: 'us-east-1' });

//**********************************
//**********************************
//Get list all users
//**********************************
//**********************************
exports.handler = async (event, context) => {
    try {
        // var groupID = event.queryStringParameters.name;
        const params = {

            TableName: 'platform4good-collaboration',
            IndexName: 'EntityBySName',
            KeyConditionExpression: '#EntityType = :value',
            ExpressionAttributeNames: {
                "#EntityType": "EntityType"
            },
            ExpressionAttributeValues: {
                ':value': 'user'
            }
        };

        var resultList = await ddb.query(params).promise();
        var response = {
            statusCode: 200,
            body: JSON.stringify(resultList),
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        };
        return response;

    } catch (error) {
        console.error(error);
    }
};

// RESPONSE SAMPLE:
// {
//   "statusCode": 200,
//   "body": "{\"Items\":[{\"UserEmail\":\"joshy@email.com\",\"SName\":\"joshy paul\",\"SK\":\"usr-joshy\",\"Status\":\"active\",\"PK\":\"usr-joshy\",\"UserName\":\"Joshy Paul\",\"GitLabInfo\":\"{         \\\"id\\\": 6523408,         \\\"name\\\": \\\"Joshy Paul\\\",         \\\"username\\\": \\\"joshy.paul.c\\\",         \\\"state\\\": \\\"active\\\",         \\\"avatar_url\\\": \\\"https://secure.gravatar.com/avatar/0154020855247b987ae90ce379ecd372?s=80&d=identicon\\\",         \\\"web_url\\\": \\\"https://gitlab.com/joshy.paul.c\\\"     }\",\"EntityType\":\"user\",\"Tenent\":\"tnt-001\",\"Name\":\"Joshy Paul\"},{\"UserEmail\":\"nilesh@email.com\",\"SName\":\"nilesh shah\",\"SK\":\"usr-nilesh\",\"Status\":\"active\",\"PK\":\"usr-nilesh\",\"UserName\":\"Nilesh Shah\",\"GitLabInfo\":\" {         \\\"id\\\": 6493059,         \\\"name\\\": \\\"Nilesh Shah\\\",         \\\"username\\\": \\\"nilesh.nitish.shah\\\",         \\\"state\\\": \\\"active\\\",         \\\"avatar_url\\\": \\\"https://secure.gravatar.com/avatar/0a73a44df9fdf800207ceb59bb5d14d8?s=80&d=identicon\\\",         \\\"web_url\\\": \\\"https://gitlab.com/nilesh.nitish.shah\\\"     }\",\"EntityType\":\"user\",\"Tenent\":\"tnt-001\",\"Name\":\"Nilesh Shah\"}],\"Count\":2,\"ScannedCount\":2}"
// }
