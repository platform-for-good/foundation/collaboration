const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient({ region: 'us-east-1' });
var lambda = new AWS.Lambda({ region: 'us-east-1' });
const uniqueProId = 'pro-' + (new Date()).getTime().toString(36);

exports.handler = async (event, context) => {
    var project = JSON.parse(event.body);
    var projectID = uniqueProId;
    var projectName = project.name;
    var projectDescription = project.description;
    var projectDomain = project.domain;
    var customInfo = project.custominfo;
    var groupID = project.groupid;
    var gitLabGroupId = project.gitlabgroupid;
    var creatorUserId = 'usr-' + project.userid; //joshy.paul.c@gmail.com
    var creatorUserGitLabId = project.gitlabuserid; //6493059
    var access_level = 40;


    //1.retreive creatorUser from collaboration to get the creator GitLabId
    const creatorUser = await ddb.get({
        TableName: "platform4good-collaboration",
        Key: {
            "PK": creatorUserId,
            "SK": creatorUserId
        }
    }).promise();

    //2.retreive Group from collaboration  to get the group GitLabId
    const createdinGroup = await ddb.get({
        TableName: "platform4good-collaboration",
        Key: {
            "PK": groupID,
            "SK": groupID
        }
    }).promise();

    //3.create gitlab project 4.added creator as owner
    const gitLabProjectToCreate = {
        "operation": "projects.create",
        "name": projectName,
        "description": projectDescription,
        "gitlabgroupid": gitLabGroupId,
        "gitlabuserid": creatorUserGitLabId //6493059
    };

    const createdGitLabProject = await lambda.invoke({
        FunctionName: 'p4g-tcoe-sharedservice-gitlabservice',
        Payload: JSON.stringify(gitLabProjectToCreate)
    }).promise();
    const createdGitLabProjectPayload = JSON.parse(createdGitLabProject.Payload);
    const createdGitLabProjectPayloadErrorType = createdGitLabProjectPayload.errorType;

    if (createdGitLabProjectPayloadErrorType > "") {
        var response = {
            statusCode: 500,
            body: JSON.stringify(createdGitLabProjectPayload.errorMessage),
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        };
        return response;
    }
    //5.create Project record in collaboration dynamodb.                                   
    const collaborationgProjectToCreate = {
        TableName: 'platform4good-collaboration',
        Item: {
            'PK': groupID,
            'SK': projectID,
            'EntityType': 'project',
            'Name': projectName,
            'Description': projectDescription,
            'Status': 'approved',
            'Type': 'foundation',
            'ProjectName': projectName,
            'ProjectDomain': projectDomain,
            'CustomInfo': customInfo,
            'SName': projectName.toLowerCase(),
            'GitLabInfo': createdGitLabProject.Payload
        }
    };
    await ddb.put(collaborationgProjectToCreate).promise();

    //5.retreive created Collaboration Project
    /*const createdCollaborationProject = await ddb.get({
              TableName: "platform4good-collaboration",
              Key: { "PK": groupID,
                     "SK": projectID }
            }).promise();
    console.log(createdCollaborationProject);  */

    //6.Add Creator as project-member with owner role to Collaboration Group
    const collaborationgProjectMemberToAdd = {
        TableName: 'platform4good-collaboration',
        Item: {
            'PK': groupID + "#" + projectID,
            'SK': creatorUserId,
            'EntityType': 'project-member',
            'ProjectName': projectName,
            'UserName': creatorUser.Item.Name,
            'UserEmail': creatorUser.Item.UserEmail,
            'MemberRole': access_level
        }
    };

    await ddb.put(collaborationgProjectMemberToAdd).promise();

    var response = {
        statusCode: 200,
        body: JSON.stringify("Project created successfully"),
        headers: {
            'Access-Control-Allow-Origin': '*'
        }

    };

    return response;
};

