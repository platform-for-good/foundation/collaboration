const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient({ region: 'us-east-1' });
var lambda = new AWS.Lambda({ region: 'us-east-1' });

exports.handler = async (event, context) => {

    var group = JSON.parse(event.body);
    var access_level = 40; //40
    var userid = 'usr-' + group.userid; //nilesh
    var groupid = group.groupid;
    var gitlabuserid = group.gitlabuserid;
    var gitlabgroupid = group.gitlabgroupid;


    //1.retreive user from collaboration
    const creatorUser = await ddb.get({
        TableName: "platform4good-collaboration",
        Key: {
            "PK": userid,
            "SK": userid
        }
    }).promise();

    //2.retreive group from collaboration
    const createdInGroup = await ddb.get({
        TableName: "platform4good-collaboration",
        Key: {
            "PK": groupid,
            "SK": groupid
        }
    }).promise();

    //3.added group member - shared service lambda call
    const gitLabGroupMemberToCreate = {
        "operation": "groupmembers.create",
        "access-level": access_level,
        "gitlabgroupid": gitlabgroupid,
        "gitlabuserid": gitlabuserid
    };

    const createdGitLabGroupMember = await lambda.invoke({
        FunctionName: 'p4g-tcoe-sharedservice-gitlabservice',
        Payload: JSON.stringify(gitLabGroupMemberToCreate)
    }).promise();
    const createdGitLabGroupMemberPayload = JSON.parse(createdGitLabGroupMember.Payload);
    const createdGitLabGroupMemberPayloadErrorType = createdGitLabGroupMemberPayload.errorType;

    if (createdGitLabGroupMemberPayloadErrorType > "") {
        var response = {
            statusCode: 500,
            body: JSON.stringify(createdGitLabGroupMemberPayload.errorMessage),
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        };
        return response;
    }



    //6.Add Creator as group-member with owner role to Collaboration Group
    const collaborationgGroupMemberToAdd = {
        TableName: 'platform4good-collaboration',
        Item: {
            'PK': groupid,
            'SK': userid,
            'EntityType': 'group-member',
            'GroupName': createdInGroup.Item.Name,
            'UserName': creatorUser.Item.Name,
            'UserEmail': creatorUser.Item.UserEmail,
            'MemberRole': access_level
        }
    };

    await ddb.put(collaborationgGroupMemberToAdd).promise();

    var response = {
        statusCode: 200,
        body: JSON.stringify(collaborationgGroupMemberToAdd),
        headers: {
            'Access-Control-Allow-Origin': '*'
        }

    };

    return response;
};

