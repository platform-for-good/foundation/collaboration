// //**************************************************
// //**************************************************
// //Get getProjectMembers by groupID and projectID (PK)
// //**************************************************
// //**************************************************

const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient({region: 'us-east-1'}); 

exports.handler = async (event, context) => {
 try {
        console.log(`Get all project memeber Info function invoked with groupID: ${event.pathParameters.groupID}`)
        console.log(`Get all project memeber Info function invoked with projectID: ${event.pathParameters.projectID}`)
        var builtString =  event.pathParameters.groupID + '#' + event.pathParameters.projectID;
        console.log(`BuiltString: ${builtString}`)
        const params = {
        
        TableName: 'platform4good-collaboration',
        KeyConditionExpression: '#PK = :value',
		ExpressionAttributeNames: {
			"#PK": "PK"
		},		
		ExpressionAttributeValues: {
                ':value': builtString
            }	
    };  
        var resultList = await ddb.query(params).promise();
        var response = {
                        statusCode: 200,
                        body: JSON.stringify(resultList),
                        headers: {
                            'Access-Control-Allow-Origin' : '*'
                        }
                    };
		return response;
		
 } catch (error) 
 {
        console.error(error);
 }
};
