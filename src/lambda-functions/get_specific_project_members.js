const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient({ region: 'us-east-1' });

//******************
//******************
//Get getSpecificProjectMembers by projectID (PK)
//******************
//******************
exports.handler = async (event, context) => {

    try {

        var params = {
            TableName: "platform4good-collaboration",
            ScanFilter: {
                "PK": {
                    ComparisonOperator: "CONTAINS",
                    AttributeValueList: [event.pathParameters.projectid]
                }
            }
        };
        var resultList = await ddb.scan(params).promise();
        var response = {
            statusCode: 200,
            body: JSON.stringify(resultList),
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        };
        return response;
    } catch (error) {
        console.error(error);
    }
};