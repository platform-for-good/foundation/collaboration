const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient({region: 'us-east-1'}); 
//**********************************
//**********************************
//Get all projects listing 
//**********************************
//**********************************
exports.handler = async (event, context, callback) => {
 try {
     
            const params = {
        TableName: 'platform4good-collaboration',
		IndexName: 'EntityByType',
        KeyConditionExpression: '#EntityType = :value',
		ExpressionAttributeNames: {
			"#EntityType": "EntityType"
		},		
		ExpressionAttributeValues: {
                ':value': 'project'
            }		
    };    
        
        var resultList = await ddb.query(params).promise();
        var response = {
                        statusCode: 200,
                        body: JSON.stringify(resultList),
                        headers: {
                            'Access-Control-Allow-Origin' : '*'
                        }
                    };
		return response;
		
 } catch (error) 
 {
        console.error(error);
 }
};
