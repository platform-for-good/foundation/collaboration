const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient({ region: 'us-east-1' });
var lambda = new AWS.Lambda({ region: 'us-east-1' });

exports.handler = async (event, context) => {
    var currentProject = JSON.parse(event.body);
    var groupid = currentProject.groupid;
    var projectid = currentProject.projectid;
    var userid = 'usr-' + currentProject.userid;
    var gitlabprojectid = currentProject.gitlabprojectid;
    var gitlabuserid = currentProject.gitlabuserid;
    var access_level = currentProject.access_level;

    //1.retreive User from collaboration to get the GitLabId of the user who shouldbe member
    const user = await ddb.get({
        TableName: "platform4good-collaboration",
        Key: {
            "PK": userid,
            "SK": userid
        }
    }).promise();
    //const userGitLabInfo = JSON.parse(user.Item.GitLabInfo);
    //var gitlabuserid = userGitLabInfo.id;

    //2.retreive project from collaboration  to get the project GitLabId
    const project = await ddb.get({
        TableName: "platform4good-collaboration",
        Key: {
            "PK": groupid,
            "SK": projectid
        }
    }).promise();
    //const projectGitLabInfo = JSON.parse( project.Item.GitLabInfo);
    //var gitlabprojectid = projectGitLabInfo.id;

    //3.call sharedservice lambda, added user as member
    const gitLabMemberToCreate = {
        "operation": "projectmembers.create",
        "gitlabprojectid": gitlabprojectid,
        "gitlabuserid": gitlabuserid,
        "access_level": access_level
    };

    const createdGitLabMember = await lambda.invoke({
        FunctionName: 'p4g-tcoe-sharedservice-gitlabservice',
        Payload: JSON.stringify(gitLabMemberToCreate)
    }).promise();
    const createdGitLabMemberPayload = JSON.parse(createdGitLabMember.Payload);
    const createdGitLabMemberPayloadErrorType = createdGitLabMemberPayload.errorType;

    if (createdGitLabMemberPayloadErrorType > "") {
        var response = {
            statusCode: 500,
            body: JSON.stringify(createdGitLabMemberPayload.errorMessage),
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        };
        return response;
    }

    //4.Add user as project-member with the requested role to Collaboration Group
    const collaborationgProjectMemberToAdd = {
        TableName: 'platform4good-collaboration',
        Item: {
            'PK': groupid + "#" + projectid,
            'SK': userid,
            'EntityType': 'project-member',
            'ProjectName': project.Item.Name,
            'UserName': user.Item.Name,
            'UserEmail': user.Item.UserEmail,
            'MemberRole': access_level
        }
    };

    await ddb.put(collaborationgProjectMemberToAdd).promise();

    var response = {
        statusCode: 200,
        body: JSON.stringify("Member added successfully"),
        headers: {
            'Access-Control-Allow-Origin': '*'
        }

    };

    return response;
};

