// //**************************************************
// //**************************************************
// //Get specific project
// //**************************************************
// //**************************************************

const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient({ region: 'us-east-1' });

exports.handler = async (event, context) => {
 try {
        console.log(`Get specific project  Info function invoked with groupID: ${event.pathParameters.groupid}`)
        console.log(`Get specific project  Info function invoked with projectID: ${event.pathParameters.projectid}`)
        
        const params = {
        
        TableName: 'platform4good-collaboration',
        KeyConditionExpression: '#PK = :value1 AND #SK = :value2',
		ExpressionAttributeNames: {
			"#PK": "PK",
			"#SK": "SK"
		},		
		ExpressionAttributeValues: {
                ':value1': event.pathParameters.groupid,
                ':value2': event.pathParameters.projectid
            }	
    };  
        var resultList = await ddb.query(params).promise();
        var response = {
            statusCode: 200,
            body: JSON.stringify(resultList),
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        };
        return response;

    } catch (error) {
        console.error(error);
    }
};
