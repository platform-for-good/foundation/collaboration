const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient({region: 'us-east-1'}); 

exports.handler = async (event, context) => {
 try {
        console.log(`Get group function invoked with groupID: ${event.pathParameters.groupid}`)
        const params = {
        
        TableName: 'platform4good-collaboration',
        KeyConditionExpression: '#PK = :value',
		ExpressionAttributeNames: {
			"#PK": "PK"
		},		
		ExpressionAttributeValues: {
                ':value': event.pathParameters.groupid
            }	
    };  
        var resultList = await ddb.query(params).promise();
        var response = {
                        statusCode: 200,
                        body: JSON.stringify(resultList),
                        headers: {
                            'Access-Control-Allow-Origin' : '*'
                        }
                    };
		return response;
		
 } catch (error) 
 {
        console.error(error);
 }
};
