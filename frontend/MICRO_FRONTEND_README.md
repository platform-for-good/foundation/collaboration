A) Environment Setup

1) Install LTS version of NodeJS from https://nodejs.org/en/download/
2) After installation, open a terminal and type
    npm install -g @angular/cli@8.0
3) clone the MainUI project from GitLab to your local PC
4) Once the clone is completed, open a terminal and navigate to <root>\Platform-for-Good\MainUI and type
    npm i

B) Example of creating a micro frontend in multi-tenant and bootstrap to platform-for-good Main UI via page composition

1) Open a terminal and navigate to <root>\Platform-for-Good\TCoE\multi-tenant-identity-and-authentication and type (frontend is the project name you want to have)
    ng new frontend 
2) Hit enter twice when prompt
3) Once the project is generated, using text editor Open <root>\Platform-for-Good\TCoE\multi-tenant-identity-and-authentication\frontend\package.json
4) Modify the following from

    "scripts": {
        "ng": "ng",
        "start": "ng serve",
        "build": "ng build",
        "test": "ng test",
        "lint": "ng lint",
        "e2e": "ng e2e"
    }
 
to 

    "scripts": {
        "ng": "ng",
        "start": "ng serve",
        "build": "ng build",
        "test": "ng test",
        "lint": "ng lint",
        "e2e": "ng e2e",
        "genpage": "node ./scripts/generatePage"
    }

5) Navigate to <root>\Platform-for-Good\MainUI and copy the scripts folder to <root>\Platform-for-Good\TCoE\multi-tenant-identity-and-authentication\frontend
6) Open a terminal and navigate to <root>\Platform-for-Good\TCoE\multi-tenant-identity-and-authentication\frontend and type (test is the page you want to generate)
    npm run genpage -- -r test
7) Navigate to <root>\TCoE\multi-tenant-identity-and-authentication\frontend\src\app\pages and copy test-page folder to <root>\Platform-for-Good\MainUI\src\app\pages
8) Open <root>\Platform-for-Good\MainUI\src\app\nav-routing.ts with an IDE or text editor
9) Add your page to this content

    export const navRoutes: NavRoute[] = [
        {
            data: { title: 'Home' },
            icon: 'home',
            path: 'home',
            loadChildren: () =>
                    import('./pages/home-page/home-page.module').then(
                    m => m.HomePageModule,
                    ),
        },
        {
            path: '',
            redirectTo: 'home',
            pathMatch: 'full',
        },
        {
            data: { title: 'Projects' },
            icon: 'assignment',
            group: '',
            path: 'projects',
            loadChildren: () =>
                    import('./pages/projects-page/projects-page.module').then(
                    m => m.ProjectsPageModule,
                    ),
        },
        {
            data: { title: 'Test' },
            icon: 'assignment',
            group: '',
            path: 'test',
            loadChildren: () =>
                    import('./pages/test-page/test-page.module').then(
                    m => m.TestPageModule,
                    ),
        }];