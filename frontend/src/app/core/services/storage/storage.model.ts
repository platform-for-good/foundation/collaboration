export enum StorageKey {
    AUTH_TOKEN = 'AUTH_TOKEN',
    ACCESS_TOKEN = 'ACCESS_TOKEN',
    CREATE_NEW_USER = 'CREATE_NEW_USER',
    CURRENT_USER = "CURRENT_USER"
}
