export interface User {
    pk: string,
    sk: string,
    tenantId: string,
    username: string,
    gitLabId?: string,
    gitLabUsername?: string,
    name: string,
    email: string,
    description?: string,
    phone?: string,
    availability?: any
    interests?: any,
    skillset?: any
    projects?: { name: string }[]
}
