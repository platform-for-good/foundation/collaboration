export interface Project {
    pk: string;
    sk: string;
    status?: string;
    gitLabId?: string,
    organizationId: string,
    organizationGitLabId?: string,
    name: string;
    allGoals?: any;
    requiredSkills?: any;
    url?: string;
    api?: string;
    visibility?: string;
    icon?: string;
    description: string;
    numberOfLikes?: string[];
    canLike: boolean;
}
