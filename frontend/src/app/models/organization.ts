export interface Organization {
    pk: string,
    sk: string,
    name: string,
    icon?: string,
    description: string,
    url?: string,
    gitLabId?: string,
    gitLabParentId?: string
}
