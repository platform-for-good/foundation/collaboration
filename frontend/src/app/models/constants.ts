export const GOALS: {
    title: string,
    value: number,
    img: string
}[] =
    [
        {
            title: 'No Poverty',
            value: 1,
            img: 'assets/1-goal.PNG'
        },
        {
            title: 'Zero Hunger',
            value: 2,
            img: 'assets/2-goal.PNG'
        },
        {
            title: 'Good Health and Well-Being',
            value: 3,
            img: 'assets/3-goal.PNG'
        },
        {
            title: 'Quality Education',
            value: 4,
            img: 'assets/4-goal.PNG'
        },
        {
            title: 'Gender Equality',
            value: 5,
            img: 'assets/5-goal.PNG'
        },
        {
            title: 'Clean Water and Sanitation',
            value: 6,
            img: 'assets/6-goal.PNG'
        },
        {
            title: 'Affordable and Clean Energy',
            value: 7,
            img: 'assets/7-goal.PNG'
        },
        {
            title: 'Decent Work and Economic Growth',
            value: 8,
            img: 'assets/8-goal.PNG'
        },
        {
            title: 'Industry, Innovation and Infrastructure',
            value: 9,
            img: 'assets/9-goal.PNG'
        },
        {
            title: 'Reduced Inequalities',
            value: 10,
            img: 'assets/10-goal.PNG'
        },
        {
            title: 'Sustainable Cities and Communities',
            value: 11,
            img: 'assets/11-goal.PNG'
        },
        {
            title: 'Responsible Consumption and Production',
            value: 12,
            img: 'assets/12-goal.PNG'
        },
        {
            title: 'Climate Action',
            value: 13,
            img: 'assets/13-goal.PNG'
        },
        {
            title: 'Life Below Water',
            value: 14,
            img: 'assets/14-goal.PNG'
        },
        {
            title: 'Life On Land',
            value: 15,
            img: 'assets/15-goal.PNG'
        },
        {
            title: 'Peace, Justice and Strong Institution',
            value: 16,
            img: 'assets/16-goal.PNG'
        },
        {
            title: 'Partnerships for the Goals',
            value: 17,
            img: 'assets/17-goal.PNG'
        }
    ];

export const SKILLS: {
    title: string,
    value: number
}[] = [
        {
            title: 'Product Owner',
            value: 1
        },
        {
            title: 'Frontend Developer',
            value: 2
        },
        {
            title: 'Cloud Engineer',
            value: 3
        },
        {
            title: 'DevOps Engineer',
            value: 4
        },
        {
            title: 'QA Engineer',
            value: 5
        },
        {
            title: 'UI/UX Engineer',
            value: 6
        }
    ];

export const MIN_HOURS = 0;
export const MAX_HOURS = 20;