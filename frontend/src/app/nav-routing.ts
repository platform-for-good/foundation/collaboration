import { Route, Router } from '@angular/router';
import { Injectable } from '@angular/core';

export interface NavRoute extends Route {
    path?: string;
    icon?: string;
    group?: string;
    groupedNavRoutes?: NavRoute[];
}

export const sideNavPath = 'nav';

export const navRoutes: NavRoute[] = [
    {
        data: { title: 'Home' },
        icon: 'home',
        path: 'home',
        loadChildren: () =>
            import('./pages/home-page/home-page.module').then(
                m => m.HomePageModule,
            ),
    },
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
    },
    {
        data: { title: 'Profile' },
        icon: 'person',
        path: 'profile',
        loadChildren: () =>
            import('./pages/profile-page/profile-page.module').then(
                m => m.ProfilePageModule,
            ),
    },
    {
        data: { title: 'Sponsors' },
        icon: 'work',
        path: 'groups',
        loadChildren: () =>
            import('./pages/groups-page/groups-page.module').then(
                m => m.GroupsPageModule,
            ),
    },
    {
        data: { title: 'Find a Project' },
        icon: 'search',
        path: 'search-projects',
        loadChildren: () =>
            import('./pages/search-projects-page/search-projects-page.module').then(
                m => m.SearchProjectsPageModule,
            ),
    },
    {
        data: { title: 'Find Your Teammates' },
        icon: 'people',
        path: 'matchmaker',
        loadChildren: () =>
            import('./pages/matchmaker-page/matchmaker-page.module').then(
                m => m.MatchmakerPageModule,
            ),
    },
];

@Injectable({
    providedIn: 'root',
})
export class NavRouteService {
    navRoute: Route;
    navRoutes: NavRoute[];

    constructor(router: Router) {
        this.navRoute = router.config.find(route => route.path === sideNavPath);
        this.navRoutes = this.navRoute.children
            .filter(route => route.data && route.data.title)
            .reduce((groupedList: NavRoute[], route: NavRoute) => {
                if (route.group) {
                    const group: NavRoute = groupedList.find(navRoute => {
                        return (
                            navRoute.group === route.group &&
                            navRoute.groupedNavRoutes !== undefined
                        );
                    });
                    if (group) {
                        group.groupedNavRoutes.push(route);
                    } else {
                        groupedList.push({
                            group: route.group,
                            groupedNavRoutes: [route],
                        });
                    }
                } else {
                    groupedList.push(route);
                }
                return groupedList;
            }, []);
    }

    public getNavRoutes(): NavRoute[] {
        return this.navRoutes;
    }
}
