import { NgModule } from '@angular/core';
import {
    Routes,
    RouterModule,
    PreloadAllModules,
    RouteReuseStrategy,
} from '@angular/router';
import { navRoutes, sideNavPath } from './nav-routing';
import { NavComponent } from './core/components/nav/nav.component';
import { AuthGuard } from './auth/auth.guard';
import { CustomRouteReuseStrategy } from './core/nav-reuse-strategy';
import { NavGuard } from './core/nav.guard';

const routes: Routes = [
    {
        path: 'login',
        loadChildren: () =>
            import('./pages/login-page/login-page.module').then(
                m => m.LoginPageModule,
            ),
    },
    {
        path: 'confirmation',
        loadChildren: () =>
            import('./pages/confirmation-page/confirmation-page.module').then(
                m => m.ConfirmationPageModule,
            ),
    },
    {
        path: 'register',
        loadChildren: () =>
            import('./pages/register-page/register-page.module').then(
                m => m.RegisterPageModule,
            ),
    },
    {
        path: 'register-success',
        loadChildren: () =>
            import('./pages/register-success-page/register-success-page.module').then(
                m => m.RegisterSuccessPageModule,
            ),
    },
    {
        path: sideNavPath,
        component: NavComponent,
        children: navRoutes,
        canActivate: [AuthGuard],
        canActivateChild: [NavGuard],
    },
    {
        path: '**',
        redirectTo: 'login',
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
    ],
    exports: [RouterModule],
    providers: [
        { provide: RouteReuseStrategy, useClass: CustomRouteReuseStrategy },
    ],
})

export class AppRoutingModule { }
