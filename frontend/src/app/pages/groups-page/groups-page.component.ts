import { Component, OnInit } from '@angular/core';
import { LambdaService } from 'src/app/lambda/lambda.service';
import { Organization } from 'src/app/models/organization';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-groups-page',
  templateUrl: './groups-page.component.html',
  styleUrls: ['./groups-page.component.scss']
})
export class GroupsPageComponent implements OnInit {

  ORGANIZATION = 'assets/Sponsor.jpg';
  organizations: Organization[];
  // user: User;
  hasGitLabAccount: boolean;

  constructor(public lambdaService: LambdaService) { }

  ngOnInit() {

    this.hasGitLabAccount = false;
    this.organizations = [];

    this.lambdaService.getAllGroups().then(organizations => {
      for (let i = 0; i < organizations.length; i++) {
        const organization = organizations[i];
        organization.icon = this.ORGANIZATION;
        this.organizations.push(organization);
      }
    });

    this.lambdaService.getUser().then(user => {
      this.hasGitLabAccount = user.gitLabId !== '';
    });
  }

  isDisabled() {
    return this.hasGitLabAccount === false;
  }
}
