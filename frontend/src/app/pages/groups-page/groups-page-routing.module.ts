import { GroupsPageComponent } from './groups-page.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '', component: GroupsPageComponent,
    data: {
      shouldReuse: true,
      key: 'groups'
    }
  },
  {
    path: 'projects',
    loadChildren: () =>
      import('../projects-page/projects-page.module').then(
        m => m.ProjectsPageModule,
      ),
    data: {
      title: 'Projects',
      isChild: true
    }
  },
  {
    path: 'group',
    loadChildren: () =>
      import('../group-page/group-page.module').then(
        m => m.GroupPageModule,
      ),
    data: {
      title: 'Sponsor',
      isChild: true
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GroupsPageRoutingModule { }
