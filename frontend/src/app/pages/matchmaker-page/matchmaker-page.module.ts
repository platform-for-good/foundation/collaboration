import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatPaginatorModule,
  MatRadioModule,
  MatSelectModule,
  MatSidenavModule
} from '@angular/material';
import { MatTableModule } from '@angular/material/table';
// import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { MatchmakerPageRoutingModule } from './matchmaker-page-routing.module';
import { MatchmakerPageComponent } from './matchmaker-page.component';


@NgModule({
  declarations: [MatchmakerPageComponent],
  imports: [
    CommonModule,
    FormsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatDialogModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatPaginatorModule,
    MatRadioModule,
    MatSelectModule,
    MatSidenavModule,
    MatTableModule,
    MatchmakerPageRoutingModule,
    // Ng2SearchPipeModule
  ]
})
export class MatchmakerPageModule { }
