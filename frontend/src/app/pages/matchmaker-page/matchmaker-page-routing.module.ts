import { MatchmakerPageComponent } from './matchmaker-page.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [ {path:'',component:MatchmakerPageComponent,data:{shouldReuse:true,key:'matchmaker'}},  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MatchmakerPageRoutingModule { }
