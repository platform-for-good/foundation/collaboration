import { Component, OnInit, SkipSelf } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { LambdaService } from 'src/app/lambda/lambda.service';
import { GOALS, SKILLS } from 'src/app/models/constants';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-matchmaker-page',
  templateUrl: './matchmaker-page.component.html',
  styleUrls: ['./matchmaker-page.component.scss']
})
export class MatchmakerPageComponent implements OnInit {

  displayedColumns: string[] = ['name', 'email', 'goals', 'skills', 'description'];
  dataSource;

  title = 'Project Match Maker';
  searchText: string;

  members: {
    name: string,
    email: string,
    goals: string,
    skills: string,
    description: string
  }[];

  projects: {
    name: string,
    goals: string
  }[];

  constructor(public lambdaService: LambdaService) {
    this.searchText = "";
    this.members = [];
    // this.dataSource = new MatTableDataSource(this.members);
  }

  ngOnInit() {

    this.lambdaService.getAllUsers().then(users => {
      for (let i = 0; i < users.length; i++) {

        let user: User = users[i];

        let goals = "";
        if (user.interests && user.interests.goals) {
          for (let i = 0; i < user.interests.goals.length; i++) {
            goals += GOALS[user.interests.goals[i] - 1].title;
            if (i !== user.interests.goals.length - 1) {
              goals += ", ";
            }
          }
        }

        let skills = "";
        if (user.skillset) {
          for (let i = 0; i < SKILLS.length; i++) {
            if (user.skillset[SKILLS[i].value.toString()]) {
              skills += SKILLS[i].title + ",";
            }
          }
        }

        let member = {
          name: user.name,
          email: user.email,
          goals: goals,
          skills: skills,
          description: user.description
        }

        this.members.push(member);
      }

      this.dataSource = new MatTableDataSource(this.members);

    }).catch(error => {
      console.error(error);
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
