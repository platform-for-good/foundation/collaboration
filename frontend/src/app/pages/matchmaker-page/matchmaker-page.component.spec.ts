import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchmakerPageComponent } from './matchmaker-page.component';

describe('MatchmakerPageComponent', () => {
  let component: MatchmakerPageComponent;
  let fixture: ComponentFixture<MatchmakerPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatchmakerPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchmakerPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
