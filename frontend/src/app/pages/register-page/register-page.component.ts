import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.scss']
})
export class RegisterPageComponent implements OnInit {

  APP = 'assets/pf8-2.png';
  isDemo: boolean;
  firstName: string;
  lastName: string;
  email: string;
  errorMessage: string;

  constructor(private authService: AuthService, private router: Router) {
    this.isDemo = false;
  }

  ngOnInit() {
    this.errorMessage = '';
    if (this.authService.isLogged()) {
      this.navigateTo();
    }
  }

  public async back() {
    try {
      this.navigateTo('login');
    } catch (e) {
    }
  }

  public navigateTo(url?: string) {
    url = url || 'nav';
    this.router.navigate([url], { replaceUrl: true });
  }

  public async register() {
    try {
      let url;
      if (this.isDemo) {
        url = (await this.authService.mockRegister(
          this.email,
          this.email
        )) as string;

      } else {
        url = (await this.authService.register(
          this.firstName,
          this.lastName,
          this.email
        )) as string;

      }

      this.navigateTo(url);

    } catch (e) {
      this.errorMessage = 'Unable to register';
    }
  }
}
