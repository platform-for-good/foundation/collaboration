import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';

import { ConfirmationPageRoutingModule } from './confirmation-page-routing.module';
import { ConfirmationPageComponent } from './confirmation-page.component';

@NgModule({
  declarations: [ConfirmationPageComponent],
  imports: [
    CommonModule,
    ConfirmationPageRoutingModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatSlideToggleModule,
    FlexLayoutModule,
    FormsModule,
  ]
})
export class ConfirmationPageModule { }
