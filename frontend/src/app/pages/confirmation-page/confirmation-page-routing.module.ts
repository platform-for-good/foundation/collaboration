import { ConfirmationPageComponent } from './confirmation-page.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [ {path:'',component:ConfirmationPageComponent,data:{shouldReuse:true,key:'confirmation'}},  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfirmationPageRoutingModule { }
