import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterSuccessPageRoutingModule } from './register-success-page-routing.module';
import { RegisterSuccessPageComponent } from './register-success-page.component';


@NgModule({
  declarations: [RegisterSuccessPageComponent],
  imports: [
    CommonModule,
    RegisterSuccessPageRoutingModule
  ]
})
export class RegisterSuccessPageModule { }
