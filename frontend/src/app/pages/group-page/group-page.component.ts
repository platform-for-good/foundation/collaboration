import { AfterContentChecked, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LambdaService } from 'src/app/lambda/lambda.service';
import { Organization } from 'src/app/models/organization';

@Component({
  selector: 'app-group-page',
  templateUrl: './group-page.component.html',
  styleUrls: ['./group-page.component.scss']
})
export class GroupPageComponent implements OnInit, AfterContentChecked {

  detailsFormGroup: FormGroup;
  organization: Organization;
  message: string;

  constructor(public lambdaService: LambdaService, private formBuilder: FormBuilder) {
    this.organization = history.state.data;
    this.organization.name = 'New Sponsor';
    this.organization.description = 'Be a sponsor today!';
  }

  ngOnInit() {
    this.detailsFormGroup = this.formBuilder.group({
      name: ['', Validators.required],
      description: [''],
    });
  }

  ngAfterContentChecked() {
  }

  submit() {
    this.lambdaService.createGroup(this.organization).then(message => {
      
      this.message = this.organization.name + ' is successfully created.';
      setTimeout(() => {
        this.message = '';
      }, 5000);

    }).catch(error => {

      this.message = 'Failed to create ' + this.organization.name;
      setTimeout(() => {
        this.message = '';
      }, 5000);

    });
  }
}
