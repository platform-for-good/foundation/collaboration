import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';

import { LoginPageRoutingModule } from './login-page-routing.module';
import { LoginPageComponent } from './login-page.component';

@NgModule({
    declarations: [LoginPageComponent],
    imports: [
        CommonModule,
        LoginPageRoutingModule,
        // LogoModule,
        MatCardModule,
        MatInputModule,
        MatButtonModule,
        MatSlideToggleModule,
        FlexLayoutModule,
        FormsModule,
    ],
})
export class LoginPageModule { }
