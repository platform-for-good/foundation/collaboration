import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent implements OnInit {
    APP = 'assets/pf8-2.png';
    isDemo: boolean;
    email: string;
    password: string;
    errorMessage: string;

    constructor(private authService: AuthService, private router: Router) {
        this.isDemo = false;
    }

    ngOnInit() {
        this.errorMessage = '';
        if (this.authService.isLogged()) {
            this.navigateTo();
        }
    }

    public async login() {
        try {
            let url;
            if (this.isDemo) {
                url = await this.authService.mockLogin(
                    this.email,
                    this.password,
                );

            } else {
                url = await this.authService.login(
                    this.email,
                    this.password,
                );
            }

            this.navigateTo(url);

        } catch (e) {
            this.errorMessage = 'Wrong Credentials!';
        }
    }

    public async register() {
        try {
            this.navigateTo('register');
        } catch (e) {
        }
    }

    public navigateTo(url?: string) {
        url = url || 'nav';
        this.router.navigate([url], { replaceUrl: true });
    }
}
