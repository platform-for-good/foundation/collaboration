import { AfterContentChecked, Component, DoCheck, OnInit } from '@angular/core';
import { LambdaService } from 'src/app/lambda/lambda.service';
import { GOALS, SKILLS } from 'src/app/models/constants';
import { Project } from 'src/app/models/project';

@Component({
  selector: 'app-search-projects-page',
  templateUrl: './search-projects-page.component.html',
  styleUrls: ['./search-projects-page.component.scss']
})
export class SearchProjectsPageComponent implements OnInit, DoCheck, AfterContentChecked {

  PROJECTS = 'assets/projects.jpg';
  projects: Project[];
  searchableProjects: {
    id: number,
    name: string,
    goals: string,
    skills: string,
    description: string,
    numberOfLikes?: string[],
    canLike: boolean
  }[];

  searchProject: string;

  constructor(public lambdaService: LambdaService) {
    this.searchProject = '';
  }

  ngOnInit() {
    this.populate();
  }

  ngDoCheck() {
  }

  ngAfterContentChecked() {
  }

  populate() {
    this.projects = [];
    this.searchableProjects = [];
    this.lambdaService.getAllProjects().then(projects => {
      for (let i = 0; i < projects.length; i++) {
        const project: Project = projects[i];

        project.icon = this.PROJECTS;
        this.projects.push(project);

        if (project.visibility === 'c') {

          let goals = "";
          if (project.allGoals) {

            for (let i = 0; i < project.allGoals.goals.length; i++) {
              goals += GOALS[project.allGoals.goals[i] - 1].title;
              if (i !== project.allGoals.goals.length - 1) {
                goals += ", ";
              }
            }
          }

          let skills = "";
          if (project.requiredSkills) {

            for (let i = 0; i < project.requiredSkills.skills.length; i++) {
              skills += SKILLS[project.requiredSkills.skills[i] - 1].title;
              if (i !== project.requiredSkills.skills.length - 1) {
                skills += ", ";
              }
            }
          }

          let searchableProject = {
            id: i,
            name: project.name,
            goals: goals,
            skills: skills,
            description: project.description,
            icon: project.icon,
            url: project.url,
            api: project.api,
            numberOfLikes: project.numberOfLikes,
            canLike: project.canLike
          };

          this.searchableProjects.push(searchableProject);
        }
      }
    });
  }

  like(project: Project) {
    // project.numberOfLikes++;
    alert("TODO");
  }

  join(project: {
    id: number,
    name: string,
    goals: string,
    skills: string,
    description: string,
    numberOfLikes?: number;
  }) {

    let currentProject = this.projects[project.id];

    this.lambdaService.joinProject(currentProject).then(message => {
      alert(message);

    }).catch(error => {
      alert(error);
      
    });
  }
}
