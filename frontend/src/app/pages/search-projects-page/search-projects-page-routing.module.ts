import { SearchProjectsPageComponent } from './search-projects-page.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [ {path:'',component:SearchProjectsPageComponent,data:{shouldReuse:true,key:'search-projects'}},  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchProjectsPageRoutingModule { }
