import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchProjectsPageComponent } from './search-projects-page.component';

describe('SearchProjectsPageComponent', () => {
  let component: SearchProjectsPageComponent;
  let fixture: ComponentFixture<SearchProjectsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchProjectsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchProjectsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
