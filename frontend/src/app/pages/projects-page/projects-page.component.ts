import { AfterContentChecked, Component, OnInit } from '@angular/core';
import { LambdaService } from 'src/app/lambda/lambda.service';
import { Organization } from 'src/app/models/organization';
import { Project } from 'src/app/models/project';

// https://stackblitz.com/edit/angular-mat-stepper-full-height?file=app%2Fdemo%2Fdemo.component.html
// https://www.angularjswiki.com/angular/angular-material-icons-list-mat-icon-list/#mat-icon-list-category-editor

@Component({
  selector: 'app-projects-page',
  templateUrl: './projects-page.component.html',
  styleUrls: ['./projects-page.component.scss']
})
export class ProjectsPageComponent implements OnInit, AfterContentChecked {

  PROJECTS = 'assets/projects.jpg';

  projects: Project[];
  // currentOrganizationId: string;
  currentOrganization: Organization;

  constructor(public lambdaService: LambdaService) {
    // this.currentOrganizationId = '';
  }

  ngOnInit() {
  }

  ngAfterContentChecked() {
    if (history.state.data) {
      if (this.currentOrganization !== history.state.data) {
        this.currentOrganization = history.state.data;
        this.projects = [];
        this.lambdaService.getProjectsByGroup(history.state.data).then(projects => {
          this.populate(projects);
        });
      }
    }
  }

  populate(projects: Project[]) {
    this.projects = [];

    for (let i = 0; i < projects.length; i++) {
      const project: Project = projects[i];
      // TODO - for testing only
      // project.api = "assets/" + project.pk + ".html";

      project.icon = this.PROJECTS;
      this.projects.push(project);
    }

    this.projects.push({
      pk: '',
      sk: '',
      status: 'new',
      organizationId: this.currentOrganization.pk,
      organizationGitLabId: this.currentOrganization.gitLabId,
      name: 'New Project',
      icon: this.PROJECTS,
      description: `Add a new Project`,
      numberOfLikes: [],
      canLike: true
    });
  }

  submit() {
  }

  like(project: Project) {
    // project.numberOfLikes++;
    alert("TODO");
  }
}
