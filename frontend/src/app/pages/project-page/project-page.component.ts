import { Component, DoCheck, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { LambdaService } from 'src/app/lambda/lambda.service';
import { Project } from 'src/app/models/project';
import { GOALS, SKILLS } from 'src/app/models/constants';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-project-page',
  templateUrl: './project-page.component.html',
  styleUrls: ['./project-page.component.scss']
})
export class ProjectPageComponent implements OnInit, DoCheck {

  isLinear = false;
  detailsFormGroup: FormGroup;
  goalsFormGroup: FormGroup;
  skillsFormGroup: FormGroup;
  teamFormGroup: FormGroup;

  project: Project;
  currentProjectId: string;
  message: string;

  users: User[];
  // newMembers: {
  //   name: string,
  //   email: string,
  //   goals: string,
  //   skills: string
  // }[];
  teamMembers: User[];

  goals: {
    title: string,
    value: number,
    checked?: string,
    img: string
  }[];

  skills: {
    title: string,
    value: number,
    checked?: string
  }[];

  visibility: {
    value: string,
    title: string,
    options: { id: string, name: string, checked: boolean }[]
  };

  constructor(public lambdaService: LambdaService, private formBuilder: FormBuilder) {
    this.users = [];
    // this.newMembers = [];
    this.teamMembers = [];
  }

  ngOnInit() {
    this.populate();
    this.refresh();
  }

  ngDoCheck() {

    if (this.currentProjectId !== history.state.data.pk) {
      this.refresh();
    }
  }

  refresh() {
    this.project = history.state.data;
    this.currentProjectId = this.project.pk;

    this.goals = [];
    for (let i = 0; i < GOALS.length; i++) {
      const goal: {
        title: string,
        value: number,
        checked?: string,
        img: string
      } = {
        title: GOALS[i].title,
        value: GOALS[i].value,
        img: GOALS[i].img
      };

      this.goals.push(goal);
    }

    this.skills = [];
    for (let i = 0; i < SKILLS.length; i++) {
      const skill: {
        title: string,
        value: number,
        checked?: string,
      } = {
        title: SKILLS[i].title,
        value: SKILLS[i].value,
      }

      this.skills.push(skill);
    }

    this.visibility = {
      value: 'visibility',
      title: 'Visibility',
      options: [{ id: 'c', name: 'Public', checked: true }, { id: 'e', name: 'Private', checked: false }]
    }

    this.lambdaService.getAllUsers().then(users => {
      this.users = users;
      // this.updateUsers(users);

      if (this.project.status !== 'new') {

        this.lambdaService.getTeamMembersByProject(this.project).then(members => {
          this.teamMembers = members;

          if (members && members.length > 0) {
            this.users = users.filter(this.comparer(members));
            // users = users.filter(this.comparer(members));
            // this.updateUsers(users);
          }

        });
      }

      if (this.project.visibility) {
        this.detailsFormGroup.get(this.visibility.value).setValue(this.project.visibility);

        if (this.project.visibility === 'c') {
          this.visibility.options[0].checked = true;
        } else {
          this.visibility.options[1].checked = true;
        }
      } else {
        this.project.visibility = 'c';
        this.visibility.options[0].checked = true;
      }

      if (this.project.allGoals) {
        for (let i = 0; i < this.goals.length; i++) {
          for (let j = 0; j < this.project.allGoals.goals.length; j++) {

            if (this.goals[i].value === this.project.allGoals.goals[j]) {
              this.goals[i].checked = this.goals[i].value.toString();
              const goalsArray: FormArray = this.goalsFormGroup.get('goals') as FormArray;
              goalsArray.push(new FormControl(parseInt(this.goals[i].checked)));
            }
          }
        }
      }

      if (this.project.requiredSkills) {
        for (let i = 0; i < this.skills.length; i++) {
          for (let j = 0; j < this.project.requiredSkills.skills.length; j++) {

            if (this.skills[i].value === this.project.requiredSkills.skills[j]) {
              this.skills[i].checked = this.skills[i].value.toString();
              const skillsArray: FormArray = this.skillsFormGroup.get('skills') as FormArray;
              skillsArray.push(new FormControl(parseInt(this.skills[i].checked)));
            }
          }
        }
      }

    });
  }

  // updateUsers(users: User[]) {
  //   users.forEach(user => {
  //     let newMember = {
  //       name: user.name,
  //       email: user.email,
  //       goals: "",
  //       skills: ""
  //     }
  //   });
  // }

  populate() {

    this.detailsFormGroup = this.formBuilder.group({
      name: ['', Validators.required],
      url: ['',
        [Validators.pattern('^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$')]
      ],
      description: [''],
      visibility: [''] // https://www.concretepage.com/angular-material/angular-material-radio-button
    });

    this.goalsFormGroup = this.formBuilder.group({
      goals: this.formBuilder.array([])
    });

    this.skillsFormGroup = this.formBuilder.group({
      skills: this.formBuilder.array([])
    });

    this.teamFormGroup = this.formBuilder.group({
      searchProject: [''],
      teams: new FormControl()
    });
  }

  submit() {
    this.project.name = this.detailsFormGroup.value.name;
    
    if (this.detailsFormGroup.value.visibility !== "") {
      this.project.visibility = this.detailsFormGroup.value.visibility;
    }
    this.project.description = this.detailsFormGroup.value.description;
    this.project.allGoals = this.goalsFormGroup.value;
    this.project.requiredSkills = this.skillsFormGroup.value;

    if (this.project.status === 'new') {
      if (!this.project.description || this.project.description === '') {
        this.project.description = this.project.name;
      }

      this.lambdaService.createProject(this.project).then(message => {

        this.message = this.project.name + ' is successfully created.';
        setTimeout(() => {
          this.message = '';
        }, 5000);

      }).catch(error => {

        this.message = 'Failed to create ' + this.project.name;
        setTimeout(() => {
          this.message = '';
        }, 5000);
      });

    } else {
      // TODO
      alert('Under construction: ' + this.project.name + ' updated!');
    }
  }

  comparer(otherArray: User[]) {
    return function (current: User) {
      return otherArray.filter(function (other) {
        return other.pk == current.pk;
      }).length == 0;
    }
  }

  onGoalsCheckboxChange(e) {
    const goalsArray: FormArray = this.goalsFormGroup.get('goals') as FormArray;

    if (e.target.checked) {
      goalsArray.push(new FormControl(parseInt(e.target.value)));

    } else {
      let i = 0;
      goalsArray.controls.forEach((item: FormControl) => {
        if (item.value == e.target.value) {
          goalsArray.removeAt(i);
        }
        i++;
      });
    }
  }

  onSkillsCheckboxChange(e) {
    const skillsArray: FormArray = this.skillsFormGroup.get('skills') as FormArray;

    if (e.target.checked) {
      skillsArray.push(new FormControl(parseInt(e.target.value)));

    } else {
      let i = 0;
      skillsArray.controls.forEach((item: FormControl) => {
        if (item.value == e.target.value) {
          skillsArray.removeAt(i);
        }
        i++;
      });
    }
  }
}
