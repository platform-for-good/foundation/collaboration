import { HomePageComponent } from './home-page.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        data: { shouldReuse: true, key: 'home' },
        component: HomePageComponent,
    },
    // {
    //     path: 'child',
    //     loadChildren: () =>
    //         import('../child-page/child-page.module').then(
    //             m => m.ChildPageModule,
    //         ),
    //     data: { title: 'Child', isChild: true },
    // }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class HomePageRoutingModule {
}
