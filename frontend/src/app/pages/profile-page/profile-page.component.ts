import { AfterContentChecked, Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { LambdaService } from 'src/app/lambda/lambda.service';
import { GOALS, MAX_HOURS, MIN_HOURS, SKILLS } from 'src/app/models/constants';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss']
})
export class ProfilePageComponent implements OnInit, AfterContentChecked {

  MIN_HOURS: number = MIN_HOURS;
  MAX_HOURS: number = MAX_HOURS;

  isLinear = false;
  detailsFormGroup: FormGroup;
  goalsFormGroup: FormGroup;
  skillsetsFormGroup: FormGroup;
  availabilityFormGroup: FormGroup;

  user: User;
  hasGitLabAccount: boolean;
  message: string;
  goals: {
    title: string,
    value: number,
    checked?: string,
    img: string
  }[];

  skills: {
    value: number,
    title: string,
    options: { id: string, name: string, checked: boolean }[]
  }[];

  constructor(public lambdaService: LambdaService, private formBuilder: FormBuilder) {
    this.message = '';
    this.user = {
      pk: '',
      sk: '',
      tenantId: '',
      username: '',
      gitLabUsername: '',
      name: '',
      email: '',
      description: '',
      phone: '',
      availability: {
        weekendEvening: 0,
        weekendAfternoon: 0,
        weekendMorning: 0,
        weekdayEvening: 0,
        weekdayAfternoon: 0,
        weekdayMorning: 0,
      },
      interests: {},
      skillset: {}
    }

    this.goals = [];
    for (let i = 0; i < GOALS.length; i++) {
      const goal: {
        title: string,
        value: number,
        checked?: string,
        img: string
      } = {
        title: GOALS[i].title,
        value: GOALS[i].value,
        img: GOALS[i].img
      };

      this.goals.push(goal);
    }

    this.skills = [];
    for (let i = 0; i < SKILLS.length; i++) {
      const skill: {
        value: number,
        title: string,
        options: { id: string, name: string, checked: boolean }[]
      } = {
        value: SKILLS[i].value,
        title: SKILLS[i].title,
        options: [{ id: 'b', name: 'Beginner', checked: true }, { id: 'e', name: 'Experienced', checked: false }]
      }

      this.skills.push(skill);
    }
  }

  ngOnInit() {
    this.hasGitLabAccount = false;
    this.populate();

    this.lambdaService.getUser().then(user => {
      this.user = user;

      if (!this.user.availability) {
        this.user.availability = {
          weekendEvening: 0,
          weekendAfternoon: 0,
          weekendMorning: 0,
          weekdayEvening: 0,
          weekdayAfternoon: 0,
          weekdayMorning: 0,
        };
      }

      if (this.user.skillset) {
        for (let i = 0; i < this.skills.length; i++) {
          this.skillsetsFormGroup.get(this.skills[i].value.toString()).setValue(this.user.skillset[this.skills[i].value]);
          if (this.user.skillset[this.skills[i].value] === 'b') {
            this.skills[i].options[0].checked = true;
          } else {
            this.skills[i].options[1].checked = true;
          }
        }
      }

      if (this.user.interests && this.user.interests.goals) {
        for (let i = 0; i < this.goals.length; i++) {
          for (let j = 0; j < this.user.interests.goals.length; j++) {

            if (this.goals[i].value === this.user.interests.goals[j]) {
              this.goals[i].checked = this.goals[i].value.toString();
              const goalsArray: FormArray = this.goalsFormGroup.get('goals') as FormArray;
              goalsArray.push(new FormControl(parseInt(this.goals[i].checked)));
            }
          }
        }
      }

      if (this.user.gitLabId) {
        this.hasGitLabAccount = this.user.gitLabId !== '';
      }

    });
  }

  ngAfterContentChecked() {
  }

  populate() {
    this.detailsFormGroup = this.formBuilder.group({
      name: [this.user.name, Validators.required],
      phone: [''],
      email: ['', [Validators.required, Validators.email]],
      gitLabUsername: ['', Validators.required],
      description: ['']
    });

    this.goalsFormGroup = this.formBuilder.group({
      goals: this.formBuilder.array([])
    });

    // this.skillsetsFormGroup = this.formBuilder.group({
    //   skills: this.formBuilder.array([])
    // });

    this.skillsetsFormGroup = this.formBuilder.group({
      '1': [''],
      '2': [''],
      '3': [''],
      '4': [''],
      '5': [''],
      '6': [''],
    });

    https:// www.positronx.io/angular-checkbox-tutorial/
    this.availabilityFormGroup = this.formBuilder.group({
      weekendEvening: [''],
      weekendAfternoon: [''],
      weekendMorning: [''],
      weekdayEvening: [''],
      weekdayAfternoon: [''],
      weekdayMorning: [''],
    });
  }

  submit() {
    this.user.name = this.detailsFormGroup.value.name;
    this.user.gitLabUsername = this.detailsFormGroup.value.gitLabUsername;
    this.user.phone = this.detailsFormGroup.value.phone;
    this.user.description = this.detailsFormGroup.value.description;
    this.user.interests = this.goalsFormGroup.value;
    this.user.skillset = this.skillsetsFormGroup.value;
    this.user.availability = this.availabilityFormGroup.value;

    this.lambdaService.updateUser(this.user).then((message) => {
      this.message = message;
      setTimeout(() => {
        this.message = '';
      }, 5000);

    });
  }

  onGoalsCheckboxChange(e) {
    const goalsArray: FormArray = this.goalsFormGroup.get('goals') as FormArray;

    if (e.target.checked) {
      goalsArray.push(new FormControl(parseInt(e.target.value)));

    } else {
      let i = 0;
      goalsArray.controls.forEach((item: FormControl) => {
        if (item.value == e.target.value) {
          goalsArray.removeAt(i);
        }
        i++;
      });
    }
  }
}
