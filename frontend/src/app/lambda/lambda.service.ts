import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { CrudService } from '../core/services/http/crud.service';
import { StorageService } from '../core/services/storage/storage.service';
import { StorageKey } from '../core/services/storage/storage.model';
import { User } from '../models/user';
import { Organization } from '../models/organization';
import { Project } from '../models/project';
// import { GitLabInfo } from '../models/git-lab-info';
const { AUTH_TOKEN, CREATE_NEW_USER, CURRENT_USER } = StorageKey;

@Injectable({
  providedIn: 'root'
})
export class LambdaService extends CrudService {
  endpoint: string;
  token: string;

  constructor(http: HttpClient, private storage: StorageService) {
    super(http);
    this.url = 'https://puwyqb4m0a.execute-api.us-east-1.amazonaws.com/dev';
    this.storage.save(CURRENT_USER, null);
    this.token = this.storage.read(AUTH_TOKEN);
    this.createUser();
  }

  //#region group related

  public async createGroup(organization: Organization) {
    // https://puwyqb4m0a.execute-api.us-east-1.amazonaws.com/dev/groups
    // this.endpoint = 'groups';

    try {

      const user = await this.getUser();
      this.endpoint = 'groups';

      const payload = {
        name: organization.name,
        description: organization.description,
        userid: user.email,
        gitlabuserid: user.gitLabId
      };
      // console.log('createGroup() payload: ' + JSON.stringify(payload, null, 2));

      const response = await this.post(payload);
      // sample response
      /*
      let response = {
        "Item": {
          "SName": "spca",
          "SK": "grp-kgkq3wxe",
          "Status": "active",
          "Description": "Society for the Prevention of Cruelty to Animals (SPCA)",
          "PK": "grp-kgkq3wxe",
          "GroupName": "SPCA",
          "Type": "foundation",
          "GitLabInfo": "{\"id\":9849483,\"web_url\":\"https://gitlab.com/groups/platform-for-good/spca\",\"name\":\"SPCA\",\"path\":\"spca\",\"description\":\"Society for the Prevention of Cruelty to Animals (SPCA)\",\"visibility\":\"public\",\"share_with_group_lock\":false,\"require_two_factor_authentication\":false,\"two_factor_grace_period\":48,\"project_creation_level\":\"developer\",\"auto_devops_enabled\":null,\"subgroup_creation_level\":\"maintainer\",\"emails_disabled\":null,\"mentions_disabled\":null,\"lfs_enabled\":true,\"default_branch_protection\":2,\"avatar_url\":null,\"request_access_enabled\":true,\"full_name\":\"Platform For Good / SPCA\",\"full_path\":\"platform-for-good/spca\",\"created_at\":\"2020-10-22T11:09:36.601Z\",\"parent_id\":8748133,\"ldap_cn\":null,\"ldap_access\":null,\"shared_with_groups\":[],\"projects\":[],\"shared_projects\":[],\"shared_runners_minutes_limit\":null,\"extra_shared_runners_minutes_limit\":null,\"prevent_forking_outside_group\":null}",
          "EntityType": "group",
          "Tenent": "p4gTENANT",
          "Name": "SPCA"
        }
      }
      //*///
      // console.log('createGroup(): ' + JSON.stringify(response, null, 2));
      return response;

    } catch (e) {
      return Promise.reject(e.message);
    }
  }

  public async getAllGroups(): Promise<Organization[]> {
    // assign the API gateway endpoint
    // https://puwyqb4m0a.execute-api.us-east-1.amazonaws.com/dev/groups
    this.endpoint = 'groups';

    const payload: Organization[] = [];

    try {
      const response =
        await this.get<{
          Items: {
            SName: string,
            GroupAdminUser: string,
            SK: string,
            Status: string,
            Description: string,
            PK: string,
            GroupName: string,
            Type: string,
            GitLabInfo: string,
            EntityType: string,
            Tenent: string,
            Name: string
          }[],
          Count: number,
          ScannedCount: number
        }>();
      // console.log('getAllGroups(): ' + JSON.stringify(response, null, 2));

      // sample response
      /*
      let response = {
        "Items": [
          {
            "SName": "foundation",
            "GroupAdminUser": "",
            "SK": "grp-2",
            "Status": "active",
            "Description": "Enabler for a sustainable digital collaboration ecosystem",
            "PK": "grp-2",
            "GroupName": "Foundation",
            "Type": "foundation",
            "GitLabInfo": "{\n    \"id\": 8748494,\n    \"web_url\": \"https://gitlab.com/groups/platform-for-good/foundation\",\n    \"name\": \"Foundation\",\n    \"path\": \"foundation\",\n    \"description\": \"Platform for Good Foundation\",\n    \"visibility\": \"public\",\n    \"share_with_group_lock\": false,\n    \"require_two_factor_authentication\": false,\n    \"two_factor_grace_period\": 48,\n    \"project_creation_level\": \"developer\",\n    \"auto_devops_enabled\": null,\n    \"subgroup_creation_level\": \"maintainer\",\n    \"emails_disabled\": null,\n    \"mentions_disabled\": null,\n    \"lfs_enabled\": true,\n    \"default_branch_protection\": 2,\n    \"avatar_url\": null,\n    \"request_access_enabled\": true,\n    \"full_name\": \"Platform For Good / Foundation\",\n    \"full_path\": \"platform-for-good/foundation\",\n    \"created_at\": \"2020-08-06T09:00:03.601Z\",\n    \"parent_id\": 8748133,\n    \"ldap_cn\": null,\n    \"ldap_access\": null\n  }",
            "EntityType": "group",
            "Tenent": "p4gTENANT",
            "Name": "Foundation"
          },
          {
            "SName": "tcoe",
            "GroupAdminUser": "",
            "SK": "grp-3",
            "Status": "active",
            "Description": "Technology Center Of Excellence",
            "PK": "grp-3",
            "GroupName": "TCOE",
            "Type": "tcoe",
            "GitLabInfo": "{\n    \"id\": 8748149,\n    \"web_url\": \"https://gitlab.com/groups/platform-for-good/tcoe\",\n    \"name\": \"TCoE\",\n    \"path\": \"tcoe\",\n    \"description\": \"Technology Centre of Excellence\",\n    \"visibility\": \"public\",\n    \"share_with_group_lock\": false,\n    \"require_two_factor_authentication\": false,\n    \"two_factor_grace_period\": 48,\n    \"project_creation_level\": \"developer\",\n    \"auto_devops_enabled\": null,\n    \"subgroup_creation_level\": \"maintainer\",\n    \"emails_disabled\": null,\n    \"mentions_disabled\": null,\n    \"lfs_enabled\": true,\n    \"default_branch_protection\": 2,\n    \"avatar_url\": null,\n    \"request_access_enabled\": true,\n    \"full_name\": \"Platform For Good / TCoE\",\n    \"full_path\": \"platform-for-good/tcoe\",\n    \"created_at\": \"2020-08-06T08:30:55.652Z\",\n    \"parent_id\": 8748133,\n    \"ldap_cn\": null,\n    \"ldap_access\": null\n  }",
            "EntityType": "group",
            "Tenent": "p4gTENANT",
            "Name": "TCOE"
          },
          {
            "SName": "platform for good",
            "GroupAdminUser": "",
            "SK": "grp-1",
            "Status": "active",
            "Description": "We believe the future of business lies in being a Force-for-Good",
            "PK": "grp-1",
            "GroupName": "Platform For Good",
            "Type": "top-level",
            "GitLabInfo": "{\n    \"id\": 8748133,\n    \"web_url\": \"https://gitlab.com/groups/platform-for-good\",\n    \"name\": \"Platform For Good\",\n    \"path\": \"platform-for-good\",\n    \"description\": \"Platform for Good\",\n    \"visibility\": \"public\",\n    \"share_with_group_lock\": false,\n    \"require_two_factor_authentication\": false,\n    \"two_factor_grace_period\": 48,\n    \"project_creation_level\": \"developer\",\n    \"auto_devops_enabled\": null,\n    \"subgroup_creation_level\": \"maintainer\",\n    \"emails_disabled\": null,\n    \"mentions_disabled\": null,\n    \"lfs_enabled\": true,\n    \"default_branch_protection\": 2,\n    \"avatar_url\": null,\n    \"request_access_enabled\": true,\n    \"full_name\": \"Platform For Good\",\n    \"full_path\": \"platform-for-good\",\n    \"created_at\": \"2020-08-06T08:29:29.829Z\",\n    \"parent_id\": null,\n    \"ldap_cn\": null,\n    \"ldap_access\": null\n  }",
            "EntityType": "group",
            "Tenent": "p4gTENANT",
            "Name": "Platform For Good"
          }
        ],
        "Count": 3,
        "ScannedCount": 3
      }
      //*///

      if (response && response.Items) {
        for (let i = 0; i < response.Items.length; i++) {

          const item = response.Items[i];

          const organization: Organization = {
            pk: item.PK,
            sk: item.SK,
            name: item.Name,
            description: item.Description,
            url: ''
          };

          if (item.GitLabInfo) {
            const gitLabInfo = JSON.parse(item.GitLabInfo);
            organization.url = gitLabInfo.web_url;
            organization.gitLabId = gitLabInfo.id;
            organization.gitLabParentId = gitLabInfo.parent_id;
          }

          payload.push(organization);
        }
      }

    } catch (e) {
      return Promise.reject(e.message);
    }

    return payload;
  }

  //#endregion

  //#region project related

  public async createProject(project: Project) {
    // https://puwyqb4m0a.execute-api.us-east-1.amazonaws.com/dev/projects
    // this.endpoint = 'projects';

    try {

      const user = await this.getUser();
      this.endpoint = 'projects';
      // project.status = "approved";
      /*
      let test = {
        "name": "IQVIA Project",
        "description": "IQVIA Project",
        "domain": "Education",
        "custominfo": "kkhkhkhkhk",
        "userid": "joshy.paul.c@gmail.com",
        "gitlabuserid": 6523408,
        "groupid": "grp-kgjstn3b",
        "gitlabgroupid": 9841928
      }
      //*///
      const payload = {
        name: project.name,
        description: project.description,
        domain: project.description,
        custominfo: JSON.stringify({
          visibility: project.visibility,
          goals: project.allGoals,
          skills: project.requiredSkills,
          numberOfLikes: project.numberOfLikes
        }),
        userid: user.email,
        gitlabuserid: user.gitLabId,
        groupid: project.organizationId,
        gitlabgroupid: project.organizationGitLabId
      };
      // console.log('createProject() payload: ' + JSON.stringify(payload, null, 2));

      const response = await this.post(payload);
      // sample response
      // console.log('createProject(): ' + JSON.stringify(response, null, 2));
      return response;

    } catch (e) {
      return Promise.reject(e.message);
    }
  }

  public async getAllProjects(): Promise<Project[]> {
    // https://puwyqb4m0a.execute-api.us-east-1.amazonaws.com/dev/projects

    const payload: Project[] = [];

    try {

      const user = await this.getUser();
      this.endpoint = 'projects';

      const response =
        await this.get<{
          Items: {
            ProjectName: string,
            ProjectDomain: string,
            SName: string,
            SK: string,
            CustomInfo: string,
            Status: string,
            PK: string,
            GroupName?: string,
            Type: string,
            GitLabInfo: string,
            EntityType: string,
            Name: string
          }[],
          Count: number,
          ScannedCount: number
        }>();

      // console.log("getAllProjects(): " + JSON.stringify(response, null, 2));

      /*
      let response = {
        "Items": [
          {
            "ProjectName": "Multi Tenant Identity and Authentication",
            "ProjectDomain": "PARTNERSHIPS FOR THE GOALS",
            "SName": "multi tenant identity and authentication",
            "SK": "pro-31",
            "CustomInfo": "{\n\"Detailed Description\":\"This collaborative process works across departmental, corporate and national boundaries\",\n\"Vision of the Sponser\":\"Be a force for good\"\n}",
            "Status": "approved",
            "PK": "grp-3",
            "Type": "technology",
            "GitLabInfo": "{\n    \"id\": 20372095,\n    \"description\": \"Multi Tenant Identity & Authentication Project\",\n    \"name\": \"Multi Tenant Identity and Authentication\",\n    \"name_with_namespace\": \"Platform For Good / TCoE / Multi Tenant Identity and Authentication\",\n    \"path\": \"multi-tenant-identity-and-authentication\",\n    \"path_with_namespace\": \"platform-for-good/tcoe/multi-tenant-identity-and-authentication\",\n    \"created_at\": \"2020-08-06T08:38:42.327Z\",\n    \"default_branch\": \"master\",\n    \"tag_list\": [],\n    \"ssh_url_to_repo\": \"git@gitlab.com:platform-for-good/tcoe/multi-tenant-identity-and-authentication.git\",\n    \"http_url_to_repo\": \"https://gitlab.com/platform-for-good/tcoe/multi-tenant-identity-and-authentication.git\",\n    \"web_url\": \"https://gitlab.com/platform-for-good/tcoe/multi-tenant-identity-and-authentication\",\n    \"readme_url\": \"https://gitlab.com/platform-for-good/tcoe/multi-tenant-identity-and-authentication/-/blob/master/README.md\",\n    \"avatar_url\": null,\n    \"forks_count\": 0,\n    \"star_count\": 0,\n    \"last_activity_at\": \"2020-10-21T03:59:51.243Z\",\n    \"namespace\": {\n        \"id\": 8748149,\n        \"name\": \"TCoE\",\n        \"path\": \"tcoe\",\n        \"kind\": \"group\",\n        \"full_path\": \"platform-for-good/tcoe\",\n        \"parent_id\": 8748133,\n        \"avatar_url\": null,\n        \"web_url\": \"https://gitlab.com/groups/platform-for-good/tcoe\"\n    },\n    \"_links\": {\n        \"self\": \"https://gitlab.com/api/v4/projects/20372095\",\n        \"issues\": \"https://gitlab.com/api/v4/projects/20372095/issues\",\n        \"merge_requests\": \"https://gitlab.com/api/v4/projects/20372095/merge_requests\",\n        \"repo_branches\": \"https://gitlab.com/api/v4/projects/20372095/repository/branches\",\n        \"labels\": \"https://gitlab.com/api/v4/projects/20372095/labels\",\n        \"events\": \"https://gitlab.com/api/v4/projects/20372095/events\",\n        \"members\": \"https://gitlab.com/api/v4/projects/20372095/members\"\n    },\n    \"packages_enabled\": true,\n    \"empty_repo\": false,\n    \"archived\": false,\n    \"visibility\": \"public\",\n    \"resolve_outdated_diff_discussions\": false,\n    \"container_registry_enabled\": true,\n    \"container_expiration_policy\": {\n        \"cadence\": \"1d\",\n        \"enabled\": true,\n        \"keep_n\": 10,\n        \"older_than\": \"90d\",\n        \"name_regex\": null,\n        \"name_regex_keep\": null,\n        \"next_run_at\": \"2020-10-20T20:40:29.361Z\"\n    },\n    \"issues_enabled\": true,\n    \"merge_requests_enabled\": true,\n    \"wiki_enabled\": true,\n    \"jobs_enabled\": true,\n    \"snippets_enabled\": true,\n    \"service_desk_enabled\": true,\n    \"service_desk_address\": \"incoming+platform-for-good-tcoe-multi-tenant-identity-and-authentication-20372095-issue-@incoming.gitlab.com\",\n    \"can_create_merge_request_in\": true,\n    \"issues_access_level\": \"enabled\",\n    \"repository_access_level\": \"enabled\",\n    \"merge_requests_access_level\": \"enabled\",\n    \"forking_access_level\": \"enabled\",\n    \"wiki_access_level\": \"enabled\",\n    \"builds_access_level\": \"enabled\",\n    \"snippets_access_level\": \"enabled\",\n    \"pages_access_level\": \"enabled\",\n    \"emails_disabled\": null,\n    \"shared_runners_enabled\": true,\n    \"lfs_enabled\": true,\n    \"creator_id\": 6493059,\n    \"import_status\": \"none\",\n    \"open_issues_count\": 0,\n    \"ci_default_git_depth\": 50,\n    \"ci_forward_deployment_enabled\": true,\n    \"public_jobs\": true,\n    \"build_timeout\": 3600,\n    \"auto_cancel_pending_pipelines\": \"enabled\",\n    \"build_coverage_regex\": null,\n    \"ci_config_path\": \"\",\n    \"shared_with_groups\": [],\n    \"only_allow_merge_if_pipeline_succeeds\": false,\n    \"allow_merge_on_skipped_pipeline\": null,\n    \"request_access_enabled\": true,\n    \"only_allow_merge_if_all_discussions_are_resolved\": false,\n    \"remove_source_branch_after_merge\": true,\n    \"printing_merge_request_link_enabled\": true,\n    \"merge_method\": \"merge\",\n    \"suggestion_commit_message\": null,\n    \"auto_devops_enabled\": false,\n    \"auto_devops_deploy_strategy\": \"continuous\",\n    \"autoclose_referenced_issues\": true,\n    \"approvals_before_merge\": 0,\n    \"mirror\": false,\n    \"external_authorization_classification_label\": \"\",\n    \"marked_for_deletion_at\": null,\n    \"marked_for_deletion_on\": null,\n    \"compliance_frameworks\": [],\n    \"permissions\": {\n        \"project_access\": null,\n        \"group_access\": null\n    }\n}",
            "EntityType": "project",
            "Name": "Multi Tenant Identity and Authentication"
          },
          {
            "ProjectName": "Collaboration",
            "ProjectDomain": "PARTNERSHIPS FOR THE GOALS",
            "SName": "collaboration",
            "SK": "pro-21",
            "CustomInfo": "{\n\"Detailed Description\":\"This collaborative process works across departmental, corporate and national boundaries\",\n\"Vision of the Sponser\":\"Be a force for good\"\n}",
            "Status": "approved",
            "PK": "grp-2",
            "GroupName": "Foundation",
            "Type": "technology",
            "GitLabInfo": "{\n    \"id\": 20372599,\n    \"description\": \"Collaboration Project\",\n    \"name\": \"Collaboration\",\n    \"name_with_namespace\": \"Platform For Good / Foundation / Collaboration\",\n    \"path\": \"collaboration\",\n    \"path_with_namespace\": \"platform-for-good/foundation/collaboration\",\n    \"created_at\": \"2020-08-06T09:01:43.401Z\",\n    \"default_branch\": \"master\",\n    \"tag_list\": [],\n    \"ssh_url_to_repo\": \"git@gitlab.com:platform-for-good/foundation/collaboration.git\",\n    \"http_url_to_repo\": \"https://gitlab.com/platform-for-good/foundation/collaboration.git\",\n    \"web_url\": \"https://gitlab.com/platform-for-good/foundation/collaboration\",\n    \"readme_url\": \"https://gitlab.com/platform-for-good/foundation/collaboration/-/blob/master/README.md\",\n    \"avatar_url\": null,\n    \"forks_count\": 0,\n    \"star_count\": 0,\n    \"last_activity_at\": \"2020-10-22T01:54:23.173Z\",\n    \"namespace\": {\n        \"id\": 8748494,\n        \"name\": \"Foundation\",\n        \"path\": \"foundation\",\n        \"kind\": \"group\",\n        \"full_path\": \"platform-for-good/foundation\",\n        \"parent_id\": 8748133,\n        \"avatar_url\": null,\n        \"web_url\": \"https://gitlab.com/groups/platform-for-good/foundation\"\n    },\n    \"_links\": {\n        \"self\": \"https://gitlab.com/api/v4/projects/20372599\",\n        \"issues\": \"https://gitlab.com/api/v4/projects/20372599/issues\",\n        \"merge_requests\": \"https://gitlab.com/api/v4/projects/20372599/merge_requests\",\n        \"repo_branches\": \"https://gitlab.com/api/v4/projects/20372599/repository/branches\",\n        \"labels\": \"https://gitlab.com/api/v4/projects/20372599/labels\",\n        \"events\": \"https://gitlab.com/api/v4/projects/20372599/events\",\n        \"members\": \"https://gitlab.com/api/v4/projects/20372599/members\"\n    },\n    \"packages_enabled\": true,\n    \"empty_repo\": false,\n    \"archived\": false,\n    \"visibility\": \"public\",\n    \"resolve_outdated_diff_discussions\": false,\n    \"container_registry_enabled\": true,\n    \"container_expiration_policy\": {\n        \"cadence\": \"1d\",\n        \"enabled\": true,\n        \"keep_n\": 10,\n        \"older_than\": \"90d\",\n        \"name_regex\": null,\n        \"name_regex_keep\": null,\n        \"next_run_at\": \"2020-10-20T20:40:38.669Z\"\n    },\n    \"issues_enabled\": true,\n    \"merge_requests_enabled\": true,\n    \"wiki_enabled\": true,\n    \"jobs_enabled\": true,\n    \"snippets_enabled\": true,\n    \"service_desk_enabled\": true,\n    \"service_desk_address\": \"incoming+platform-for-good-foundation-collaboration-20372599-issue-@incoming.gitlab.com\",\n    \"can_create_merge_request_in\": true,\n    \"issues_access_level\": \"enabled\",\n    \"repository_access_level\": \"enabled\",\n    \"merge_requests_access_level\": \"enabled\",\n    \"forking_access_level\": \"enabled\",\n    \"wiki_access_level\": \"enabled\",\n    \"builds_access_level\": \"enabled\",\n    \"snippets_access_level\": \"enabled\",\n    \"pages_access_level\": \"enabled\",\n    \"emails_disabled\": null,\n    \"shared_runners_enabled\": true,\n    \"lfs_enabled\": true,\n    \"creator_id\": 6563688,\n    \"import_status\": \"none\",\n    \"open_issues_count\": 0,\n    \"ci_default_git_depth\": 50,\n    \"ci_forward_deployment_enabled\": true,\n    \"public_jobs\": true,\n    \"build_timeout\": 3600,\n    \"auto_cancel_pending_pipelines\": \"enabled\",\n    \"build_coverage_regex\": null,\n    \"ci_config_path\": \"\",\n    \"shared_with_groups\": [],\n    \"only_allow_merge_if_pipeline_succeeds\": false,\n    \"allow_merge_on_skipped_pipeline\": null,\n    \"request_access_enabled\": true,\n    \"only_allow_merge_if_all_discussions_are_resolved\": false,\n    \"remove_source_branch_after_merge\": true,\n    \"printing_merge_request_link_enabled\": true,\n    \"merge_method\": \"merge\",\n    \"suggestion_commit_message\": null,\n    \"auto_devops_enabled\": false,\n    \"auto_devops_deploy_strategy\": \"continuous\",\n    \"autoclose_referenced_issues\": true,\n    \"approvals_before_merge\": 0,\n    \"mirror\": false,\n    \"external_authorization_classification_label\": \"\",\n    \"marked_for_deletion_at\": null,\n    \"marked_for_deletion_on\": null,\n    \"compliance_frameworks\": [],\n    \"permissions\": {\n        \"project_access\": null,\n        \"group_access\": null\n    }\n}",
            "EntityType": "project",
            "Name": "Collaboration"
          },
          {
            "ProjectName": "DevOps Shared Services",
            "ProjectDomain": "PARTNERSHIPS FOR THE GOALS",
            "SName": "devops shared services",
            "SK": "pro-32",
            "CustomInfo": "{\n\"Detailed Description\":\"This collaborative process works across departmental, corporate and national boundaries\",\n\"Vision of the Sponser\":\"Be a force for good\"\n}",
            "Status": "approved",
            "PK": "grp-3",
            "Type": "technology",
            "GitLabInfo": "{\n    \"id\": 20372197,\n    \"description\": \"DevOps Shared Services Project\",\n    \"name\": \"DevOps Shared Services\",\n    \"name_with_namespace\": \"Platform For Good / TCoE / DevOps Shared Services\",\n    \"path\": \"devops-shared-services\",\n    \"path_with_namespace\": \"platform-for-good/tcoe/devops-shared-services\",\n    \"created_at\": \"2020-08-06T08:43:13.315Z\",\n    \"default_branch\": \"master\",\n    \"tag_list\": [],\n    \"ssh_url_to_repo\": \"git@gitlab.com:platform-for-good/tcoe/devops-shared-services.git\",\n    \"http_url_to_repo\": \"https://gitlab.com/platform-for-good/tcoe/devops-shared-services.git\",\n    \"web_url\": \"https://gitlab.com/platform-for-good/tcoe/devops-shared-services\",\n    \"readme_url\": \"https://gitlab.com/platform-for-good/tcoe/devops-shared-services/-/blob/master/README.md\",\n    \"avatar_url\": null,\n    \"forks_count\": 0,\n    \"star_count\": 0,\n    \"last_activity_at\": \"2020-10-21T03:59:51.341Z\",\n    \"namespace\": {\n        \"id\": 8748149,\n        \"name\": \"TCoE\",\n        \"path\": \"tcoe\",\n        \"kind\": \"group\",\n        \"full_path\": \"platform-for-good/tcoe\",\n        \"parent_id\": 8748133,\n        \"avatar_url\": null,\n        \"web_url\": \"https://gitlab.com/groups/platform-for-good/tcoe\"\n    },\n    \"_links\": {\n        \"self\": \"https://gitlab.com/api/v4/projects/20372197\",\n        \"issues\": \"https://gitlab.com/api/v4/projects/20372197/issues\",\n        \"merge_requests\": \"https://gitlab.com/api/v4/projects/20372197/merge_requests\",\n        \"repo_branches\": \"https://gitlab.com/api/v4/projects/20372197/repository/branches\",\n        \"labels\": \"https://gitlab.com/api/v4/projects/20372197/labels\",\n        \"events\": \"https://gitlab.com/api/v4/projects/20372197/events\",\n        \"members\": \"https://gitlab.com/api/v4/projects/20372197/members\"\n    },\n    \"packages_enabled\": true,\n    \"empty_repo\": false,\n    \"archived\": false,\n    \"visibility\": \"public\",\n    \"resolve_outdated_diff_discussions\": false,\n    \"container_registry_enabled\": true,\n    \"container_expiration_policy\": {\n        \"cadence\": \"1d\",\n        \"enabled\": true,\n        \"keep_n\": 10,\n        \"older_than\": \"90d\",\n        \"name_regex\": null,\n        \"name_regex_keep\": null,\n        \"next_run_at\": \"2020-10-20T20:40:31.402Z\"\n    },\n    \"issues_enabled\": true,\n    \"merge_requests_enabled\": true,\n    \"wiki_enabled\": true,\n    \"jobs_enabled\": true,\n    \"snippets_enabled\": true,\n    \"service_desk_enabled\": true,\n    \"service_desk_address\": \"incoming+platform-for-good-tcoe-devops-shared-services-20372197-issue-@incoming.gitlab.com\",\n    \"can_create_merge_request_in\": true,\n    \"issues_access_level\": \"enabled\",\n    \"repository_access_level\": \"enabled\",\n    \"merge_requests_access_level\": \"enabled\",\n    \"forking_access_level\": \"enabled\",\n    \"wiki_access_level\": \"enabled\",\n    \"builds_access_level\": \"enabled\",\n    \"snippets_access_level\": \"enabled\",\n    \"pages_access_level\": \"enabled\",\n    \"emails_disabled\": null,\n    \"shared_runners_enabled\": true,\n    \"lfs_enabled\": true,\n    \"creator_id\": 6493059,\n    \"import_status\": \"none\",\n    \"open_issues_count\": 0,\n    \"ci_default_git_depth\": 50,\n    \"ci_forward_deployment_enabled\": true,\n    \"public_jobs\": true,\n    \"build_timeout\": 3600,\n    \"auto_cancel_pending_pipelines\": \"enabled\",\n    \"build_coverage_regex\": null,\n    \"ci_config_path\": \"\",\n    \"shared_with_groups\": [],\n    \"only_allow_merge_if_pipeline_succeeds\": false,\n    \"allow_merge_on_skipped_pipeline\": null,\n    \"request_access_enabled\": true,\n    \"only_allow_merge_if_all_discussions_are_resolved\": false,\n    \"remove_source_branch_after_merge\": true,\n    \"printing_merge_request_link_enabled\": true,\n    \"merge_method\": \"merge\",\n    \"suggestion_commit_message\": null,\n    \"auto_devops_enabled\": false,\n    \"auto_devops_deploy_strategy\": \"continuous\",\n    \"autoclose_referenced_issues\": true,\n    \"approvals_before_merge\": 0,\n    \"mirror\": false,\n    \"external_authorization_classification_label\": \"\",\n    \"marked_for_deletion_at\": null,\n    \"marked_for_deletion_on\": null,\n    \"compliance_frameworks\": [],\n    \"permissions\": {\n        \"project_access\": null,\n        \"group_access\": null\n    }\n}",
            "EntityType": "project",
            "Name": "DevOps Shared Services"
          }
        ],
        "Count": 3,
        "ScannedCount": 3
      }
      //*///

      if (response && response.Items) {

        for (let i = 0; i < response.Items.length; i++) {
          const item = response.Items[i];

          const project: Project = {
            pk: item.SK,
            sk: item.SK,
            status: item.Status,
            organizationId: item.PK,
            name: item.ProjectName,
            description: item.ProjectDomain,
            numberOfLikes: [],
            canLike: true,
            visibility: 'c'
          };

          if (item.GitLabInfo) {
            const gitLabInfo = JSON.parse(item.GitLabInfo);

            project.url = gitLabInfo.web_url;
            project.gitLabId = gitLabInfo.id;
            // gitLabInfo._links.members)
            // let members = await this.getTeamMembersByProject(project);
            // console.log("members " + JSON.stringify(members, null, 2));
            // project.gitLabParentId = gitLabInfo.parent_id;
          }

          if (item.CustomInfo) {
            const custominfo = JSON.parse(item.CustomInfo);
            project.visibility = custominfo.visibility;
            project.allGoals = custominfo.goals;
            project.requiredSkills = custominfo.skills;
            project.numberOfLikes = custominfo.numberOfLikes;
            project.canLike = project.numberOfLikes.includes(user.pk) === false;

            if (custominfo.api) {
              project.api = custominfo.api;
            }
          }

          payload.push(project);
        }
      }

    } catch (e) {
      return Promise.reject(e.message);
    }

    return payload;

  }

  public async getProjectsByGroup(organization: Organization): Promise<Project[]> {
    // https://puwyqb4m0a.execute-api.us-east-1.amazonaws.com/dev/groups/{groupid}/projects
    // https://puwyqb4m0a.execute-api.us-east-1.amazonaws.com/dev/groups/{groupid}/members
    this.endpoint = 'groups';
    const payload: Project[] = [];

    try {
      const groupid = organization.pk + '/members';
      const response =
        await this.get<any>(groupid);
      // console.log(`getProjectsByGroupId(${groupid}): ` + JSON.stringify(response, null, 2));

      // sample payload
      /*
      let response = {
        "Items": [
          {
            "SName": "tcoe",
            "GroupAdminUser": "",
            "SK": "grp-3",
            "Status": "active",
            "Description": "Technology Center Of Excellence",
            "PK": "grp-3",
            "GroupName": "TCOE",
            "Type": "tcoe",
            "GitLabInfo": "{\n    \"id\": 8748149,\n    \"web_url\": \"https://gitlab.com/groups/platform-for-good/tcoe\",\n    \"name\": \"TCoE\",\n    \"path\": \"tcoe\",\n    \"description\": \"Technology Centre of Excellence\",\n    \"visibility\": \"public\",\n    \"share_with_group_lock\": false,\n    \"require_two_factor_authentication\": false,\n    \"two_factor_grace_period\": 48,\n    \"project_creation_level\": \"developer\",\n    \"auto_devops_enabled\": null,\n    \"subgroup_creation_level\": \"maintainer\",\n    \"emails_disabled\": null,\n    \"mentions_disabled\": null,\n    \"lfs_enabled\": true,\n    \"default_branch_protection\": 2,\n    \"avatar_url\": null,\n    \"request_access_enabled\": true,\n    \"full_name\": \"Platform For Good / TCoE\",\n    \"full_path\": \"platform-for-good/tcoe\",\n    \"created_at\": \"2020-08-06T08:30:55.652Z\",\n    \"parent_id\": 8748133,\n    \"ldap_cn\": null,\n    \"ldap_access\": null\n  }",
            "EntityType": "group",
            "Tenent": "p4gTENANT",
            "Name": "TCOE"
          },
          {
            "ProjectName": "Multi Tenant Identity and Authentication",
            "ProjectDomain": "PARTNERSHIPS FOR THE GOALS",
            "SName": "multi tenant identity and authentication",
            "SK": "pro-31",
            "CustomInfo": "{\"visibility\":\"c\",\"goals\":{\"goals\":[\"1-goal\",\"3-goal\"]},\"numberOfLikes\":5}",
            "Status": "approved",
            "PK": "grp-3",
            "Type": "technology",
            "GitLabInfo": "{\n    \"id\": 20372095,\n    \"description\": \"Multi Tenant Identity & Authentication Project\",\n    \"name\": \"Multi Tenant Identity and Authentication\",\n    \"name_with_namespace\": \"Platform For Good / TCoE / Multi Tenant Identity and Authentication\",\n    \"path\": \"multi-tenant-identity-and-authentication\",\n    \"path_with_namespace\": \"platform-for-good/tcoe/multi-tenant-identity-and-authentication\",\n    \"created_at\": \"2020-08-06T08:38:42.327Z\",\n    \"default_branch\": \"master\",\n    \"tag_list\": [],\n    \"ssh_url_to_repo\": \"git@gitlab.com:platform-for-good/tcoe/multi-tenant-identity-and-authentication.git\",\n    \"http_url_to_repo\": \"https://gitlab.com/platform-for-good/tcoe/multi-tenant-identity-and-authentication.git\",\n    \"web_url\": \"https://gitlab.com/platform-for-good/tcoe/multi-tenant-identity-and-authentication\",\n    \"readme_url\": \"https://gitlab.com/platform-for-good/tcoe/multi-tenant-identity-and-authentication/-/blob/master/README.md\",\n    \"avatar_url\": null,\n    \"forks_count\": 0,\n    \"star_count\": 0,\n    \"last_activity_at\": \"2020-10-21T03:59:51.243Z\",\n    \"namespace\": {\n        \"id\": 8748149,\n        \"name\": \"TCoE\",\n        \"path\": \"tcoe\",\n        \"kind\": \"group\",\n        \"full_path\": \"platform-for-good/tcoe\",\n        \"parent_id\": 8748133,\n        \"avatar_url\": null,\n        \"web_url\": \"https://gitlab.com/groups/platform-for-good/tcoe\"\n    },\n    \"_links\": {\n        \"self\": \"https://gitlab.com/api/v4/projects/20372095\",\n        \"issues\": \"https://gitlab.com/api/v4/projects/20372095/issues\",\n        \"merge_requests\": \"https://gitlab.com/api/v4/projects/20372095/merge_requests\",\n        \"repo_branches\": \"https://gitlab.com/api/v4/projects/20372095/repository/branches\",\n        \"labels\": \"https://gitlab.com/api/v4/projects/20372095/labels\",\n        \"events\": \"https://gitlab.com/api/v4/projects/20372095/events\",\n        \"members\": \"https://gitlab.com/api/v4/projects/20372095/members\"\n    },\n    \"packages_enabled\": true,\n    \"empty_repo\": false,\n    \"archived\": false,\n    \"visibility\": \"public\",\n    \"resolve_outdated_diff_discussions\": false,\n    \"container_registry_enabled\": true,\n    \"container_expiration_policy\": {\n        \"cadence\": \"1d\",\n        \"enabled\": true,\n        \"keep_n\": 10,\n        \"older_than\": \"90d\",\n        \"name_regex\": null,\n        \"name_regex_keep\": null,\n        \"next_run_at\": \"2020-10-20T20:40:29.361Z\"\n    },\n    \"issues_enabled\": true,\n    \"merge_requests_enabled\": true,\n    \"wiki_enabled\": true,\n    \"jobs_enabled\": true,\n    \"snippets_enabled\": true,\n    \"service_desk_enabled\": true,\n    \"service_desk_address\": \"incoming+platform-for-good-tcoe-multi-tenant-identity-and-authentication-20372095-issue-@incoming.gitlab.com\",\n    \"can_create_merge_request_in\": true,\n    \"issues_access_level\": \"enabled\",\n    \"repository_access_level\": \"enabled\",\n    \"merge_requests_access_level\": \"enabled\",\n    \"forking_access_level\": \"enabled\",\n    \"wiki_access_level\": \"enabled\",\n    \"builds_access_level\": \"enabled\",\n    \"snippets_access_level\": \"enabled\",\n    \"pages_access_level\": \"enabled\",\n    \"emails_disabled\": null,\n    \"shared_runners_enabled\": true,\n    \"lfs_enabled\": true,\n    \"creator_id\": 6493059,\n    \"import_status\": \"none\",\n    \"open_issues_count\": 0,\n    \"ci_default_git_depth\": 50,\n    \"ci_forward_deployment_enabled\": true,\n    \"public_jobs\": true,\n    \"build_timeout\": 3600,\n    \"auto_cancel_pending_pipelines\": \"enabled\",\n    \"build_coverage_regex\": null,\n    \"ci_config_path\": \"\",\n    \"shared_with_groups\": [],\n    \"only_allow_merge_if_pipeline_succeeds\": false,\n    \"allow_merge_on_skipped_pipeline\": null,\n    \"request_access_enabled\": true,\n    \"only_allow_merge_if_all_discussions_are_resolved\": false,\n    \"remove_source_branch_after_merge\": true,\n    \"printing_merge_request_link_enabled\": true,\n    \"merge_method\": \"merge\",\n    \"suggestion_commit_message\": null,\n    \"auto_devops_enabled\": false,\n    \"auto_devops_deploy_strategy\": \"continuous\",\n    \"autoclose_referenced_issues\": true,\n    \"approvals_before_merge\": 0,\n    \"mirror\": false,\n    \"external_authorization_classification_label\": \"\",\n    \"marked_for_deletion_at\": null,\n    \"marked_for_deletion_on\": null,\n    \"compliance_frameworks\": [],\n    \"permissions\": {\n        \"project_access\": null,\n        \"group_access\": null\n    }\n}",
            "EntityType": "project",
            "Name": "Multi Tenant Identity and Authentication"
          },
          {
            "ProjectName": "DevOps Shared Services",
            "ProjectDomain": "PARTNERSHIPS FOR THE GOALS",
            "SName": "devops shared services",
            "SK": "pro-32",
            "CustomInfo": "{\n\"Detailed Description\":\"This collaborative process works across departmental, corporate and national boundaries\",\n\"Vision of the Sponser\":\"Be a force for good\"\n}",
            "Status": "approved",
            "PK": "grp-3",
            "Type": "technology",
            "GitLabInfo": "{\n    \"id\": 20372197,\n    \"description\": \"DevOps Shared Services Project\",\n    \"name\": \"DevOps Shared Services\",\n    \"name_with_namespace\": \"Platform For Good / TCoE / DevOps Shared Services\",\n    \"path\": \"devops-shared-services\",\n    \"path_with_namespace\": \"platform-for-good/tcoe/devops-shared-services\",\n    \"created_at\": \"2020-08-06T08:43:13.315Z\",\n    \"default_branch\": \"master\",\n    \"tag_list\": [],\n    \"ssh_url_to_repo\": \"git@gitlab.com:platform-for-good/tcoe/devops-shared-services.git\",\n    \"http_url_to_repo\": \"https://gitlab.com/platform-for-good/tcoe/devops-shared-services.git\",\n    \"web_url\": \"https://gitlab.com/platform-for-good/tcoe/devops-shared-services\",\n    \"readme_url\": \"https://gitlab.com/platform-for-good/tcoe/devops-shared-services/-/blob/master/README.md\",\n    \"avatar_url\": null,\n    \"forks_count\": 0,\n    \"star_count\": 0,\n    \"last_activity_at\": \"2020-10-21T03:59:51.341Z\",\n    \"namespace\": {\n        \"id\": 8748149,\n        \"name\": \"TCoE\",\n        \"path\": \"tcoe\",\n        \"kind\": \"group\",\n        \"full_path\": \"platform-for-good/tcoe\",\n        \"parent_id\": 8748133,\n        \"avatar_url\": null,\n        \"web_url\": \"https://gitlab.com/groups/platform-for-good/tcoe\"\n    },\n    \"_links\": {\n        \"self\": \"https://gitlab.com/api/v4/projects/20372197\",\n        \"issues\": \"https://gitlab.com/api/v4/projects/20372197/issues\",\n        \"merge_requests\": \"https://gitlab.com/api/v4/projects/20372197/merge_requests\",\n        \"repo_branches\": \"https://gitlab.com/api/v4/projects/20372197/repository/branches\",\n        \"labels\": \"https://gitlab.com/api/v4/projects/20372197/labels\",\n        \"events\": \"https://gitlab.com/api/v4/projects/20372197/events\",\n        \"members\": \"https://gitlab.com/api/v4/projects/20372197/members\"\n    },\n    \"packages_enabled\": true,\n    \"empty_repo\": false,\n    \"archived\": false,\n    \"visibility\": \"public\",\n    \"resolve_outdated_diff_discussions\": false,\n    \"container_registry_enabled\": true,\n    \"container_expiration_policy\": {\n        \"cadence\": \"1d\",\n        \"enabled\": true,\n        \"keep_n\": 10,\n        \"older_than\": \"90d\",\n        \"name_regex\": null,\n        \"name_regex_keep\": null,\n        \"next_run_at\": \"2020-10-20T20:40:31.402Z\"\n    },\n    \"issues_enabled\": true,\n    \"merge_requests_enabled\": true,\n    \"wiki_enabled\": true,\n    \"jobs_enabled\": true,\n    \"snippets_enabled\": true,\n    \"service_desk_enabled\": true,\n    \"service_desk_address\": \"incoming+platform-for-good-tcoe-devops-shared-services-20372197-issue-@incoming.gitlab.com\",\n    \"can_create_merge_request_in\": true,\n    \"issues_access_level\": \"enabled\",\n    \"repository_access_level\": \"enabled\",\n    \"merge_requests_access_level\": \"enabled\",\n    \"forking_access_level\": \"enabled\",\n    \"wiki_access_level\": \"enabled\",\n    \"builds_access_level\": \"enabled\",\n    \"snippets_access_level\": \"enabled\",\n    \"pages_access_level\": \"enabled\",\n    \"emails_disabled\": null,\n    \"shared_runners_enabled\": true,\n    \"lfs_enabled\": true,\n    \"creator_id\": 6493059,\n    \"import_status\": \"none\",\n    \"open_issues_count\": 0,\n    \"ci_default_git_depth\": 50,\n    \"ci_forward_deployment_enabled\": true,\n    \"public_jobs\": true,\n    \"build_timeout\": 3600,\n    \"auto_cancel_pending_pipelines\": \"enabled\",\n    \"build_coverage_regex\": null,\n    \"ci_config_path\": \"\",\n    \"shared_with_groups\": [],\n    \"only_allow_merge_if_pipeline_succeeds\": false,\n    \"allow_merge_on_skipped_pipeline\": null,\n    \"request_access_enabled\": true,\n    \"only_allow_merge_if_all_discussions_are_resolved\": false,\n    \"remove_source_branch_after_merge\": true,\n    \"printing_merge_request_link_enabled\": true,\n    \"merge_method\": \"merge\",\n    \"suggestion_commit_message\": null,\n    \"auto_devops_enabled\": false,\n    \"auto_devops_deploy_strategy\": \"continuous\",\n    \"autoclose_referenced_issues\": true,\n    \"approvals_before_merge\": 0,\n    \"mirror\": false,\n    \"external_authorization_classification_label\": \"\",\n    \"marked_for_deletion_at\": null,\n    \"marked_for_deletion_on\": null,\n    \"compliance_frameworks\": [],\n    \"permissions\": {\n        \"project_access\": null,\n        \"group_access\": null\n    }\n}",
            "EntityType": "project",
            "Name": "DevOps Shared Services"
          }
        ],
        "Count": 3,
        "ScannedCount": 3
      }
      //*///

      if (response && response.Items) {

        const helper = new JwtHelperService();
        const decodedToken = helper.decodeToken(this.token);

        const email = decodedToken.email;

        for (let i = 0; i < response.Items.length; i++) {
          const item = response.Items[i];

          if (item.EntityType === 'project') {

            const project: Project = {
              pk: item.SK,
              sk: item.SK,
              visibility: 'public',
              status: item.Status,
              organizationId: organization.pk,
              organizationGitLabId: organization.gitLabId,
              name: item.ProjectName,
              description: item.ProjectDomain,
              numberOfLikes: [],
              canLike: true
            };

            if (item.GitLabInfo) {
              const gitLabInfo = JSON.parse(item.GitLabInfo);
              project.url = gitLabInfo.web_url;
              project.gitLabId = gitLabInfo.id;
              // project.gitLabParentId = gitLabInfo.parent_id;
            }

            if (item.CustomInfo) {
              const custominfo = JSON.parse(item.CustomInfo);
              project.visibility = custominfo.visibility;
              project.allGoals = custominfo.goals;
              project.requiredSkills = custominfo.skills;
              project.numberOfLikes = custominfo.numberOfLikes;
              project.canLike = project.numberOfLikes.includes("usr-" + email) === false;

              if (custominfo.api) {
                project.api = custominfo.api;
              }
            }

            payload.push(project);

          }
        }
      }
    } catch (e) {
      return Promise.reject(e.message);
    }

    return payload;
  }

  public async getTeamMembersByProject(project: Project): Promise<User[]> {
    // https://puwyqb4m0a.execute-api.us-east-1.amazonaws.com/dev/projects/{projectid}/members
    this.endpoint = 'projects';

    const payload: User[] = [];

    try {

      const helper = new JwtHelperService();
      const decodedToken = helper.decodeToken(this.token);

      const email = decodedToken.email;

      const projectid = project.pk + '/members';
      const response =
        await this.get<{
          Items: [
            {
              ProjectName: string,
              SK: string,
              MemberRole: number,
              PK: string,
              UserName: string,
              EntityType: string
            }
          ],
          Count: number,
          ScannedCount: number
        }>(projectid);
      // console.log(`getTeamMembersByProject(${projectid}): ` + JSON.stringify(response, null, 2));

      // sample payload
      /*
      let response = {
        "Items": [
          {
            "ProjectName": "Collaboration",
            "SK": "usr-neaven77@gmail.com",
            "MemberRole": 40,
            "PK": "grp-2#pro-21",
            "UserName": "Yong Kheng Seo",
            "EntityType": "project-member"
          }
        ],
        "Count": 1,
        "ScannedCount": 10
      }
      //*///      

      if (response && response.Items) {
        for (let i = 0; i < response.Items.length; i++) {
          const item = response.Items[i];

          const user: User = {
            pk: item.SK,
            sk: item.SK,
            tenantId: decodedToken['custom:tenant_id'],
            username: item.UserName,
            name: item.UserName,
            email: email
          }

          payload.push(user);
        }
      }

    } catch (e) {
      return Promise.reject(e.message);
    }

    return payload;
  }

  public async joinProject(project: Project) {
    // https://puwyqb4m0a.execute-api.us-east-1.amazonaws.com/dev/projects/{projectid}/members

    try {

      const user = await this.getUser();
      this.endpoint = "projects/" + project.pk + "/members";

      const payload = {
        userid: user.email,
        gitlabuserid: user.gitLabId,
        projectid: project.pk,
        gitlabprojectid: project.gitLabId,
        groupid: project.organizationId,
        access_level: 30
      }
      // console.log('joinProject() payload: ' + JSON.stringify(payload, null, 2));

      const response = await this.post(payload);
      // console.log('joinProject(): ' + JSON.stringify(response, null, 2));

      return response;

    } catch (e) {
      return Promise.reject(e.message);
    }
  }

  //#endregion

  //#region user related
  public async createUser() {
    // https://puwyqb4m0a.execute-api.us-east-1.amazonaws.com/dev/users
    this.endpoint = 'users';
    if (this.storage.read(CREATE_NEW_USER)) {

      try {

        const helper = new JwtHelperService();
        const decodedToken = helper.decodeToken(this.token);

        const email = decodedToken.email;
        const payload = {
          userid: email,
          name: decodedToken.given_name + ' ' + decodedToken.family_name,
          email,
          description: '',
          tenent: decodedToken['custom:tenant_id']
        };

        const response = await this.post(payload);
        // console.log("createUser(): " + JSON.stringify(response, null, 2));
        this.storage.remove(CREATE_NEW_USER);

      } catch (e) {
        return Promise.reject(e.message);
      }
    }
  }

  public async updateUser(user: User) {
    // https://puwyqb4m0a.execute-api.us-east-1.amazonaws.com/dev/users/{userid}
    this.endpoint = 'users';
    try {

      const payload = {
        userid: user.email,
        name: user.name,
        email: user.email,
        description: user.description,
        tenent: user.tenantId,
        gitlabusername: user.gitLabUsername,
        custominfo: JSON.stringify({
          availability: user.availability,
          interests: user.interests,
          skills: user.skillset,
          phone: user.phone
        })
      };

      const userId = 'usr-' + payload.userid;
      // console.log(`updateUser(${userId}): ` + JSON.stringify(payload, null, 2));
      const response = await this.patch(userId, payload);
      // console.log(JSON.stringify(response, null, 2));
      this.storage.save(CURRENT_USER, null);

      return response;

    } catch (e) {
      return Promise.reject(e.message);
    }
  }

  public async getUser(): Promise<User> {
    // https://puwyqb4m0a.execute-api.us-east-1.amazonaws.com/dev/users/{userid}
    this.endpoint = 'users';

    const currentLoginUser = this.storage.read(CURRENT_USER);
    if (currentLoginUser !== null) {
      return currentLoginUser;
    }

    const helper = new JwtHelperService();
    const decodedToken = helper.decodeToken(this.token);

    const email = decodedToken.email;
    const userId = 'usr-' + email;

    let payload: User = {
      pk: userId,
      sk: userId,
      tenantId: decodedToken['custom:tenant_id'],
      username: email,
      gitLabUsername: '',
      name: decodedToken.given_name + ' ' + decodedToken.family_name,
      email: email,
      description: 'Tell us about yourself',
      phone: ''
    }

    try {
      const response =
        await this.get<any>(userId);
      // console.log(`getUser(${userId}): ` + JSON.stringify(response, null, 2));

      // sample payload
      /*
      let response = {
        "Items": [
          {
            "ProjectName": "DevOps Shared Services",
            "MemberRole": 30,
            "UserEmail": "neaven77@gmail.com",
            "SK": "usr-neaven77@gmail.com",
            "PK": "grp-3#pro-32",
            "UserName": "Yong Kheng Seo",
            "EntityType": "project-member"
          },
          {
            "SName": "yong kheng seo",
            "UserEmail": "neaven77@gmail.com",
            "SK": "usr-neaven77@gmail.com",
            "CustomInfo": "{\"availability\":{\"weekendEvening\":2,\"weekendAfternoon\":3,\"weekendMorning\":0,\"weekdayEvening\":2,\"weekdayAfternoon\":4,\"weekdayMorning\":0},\"interests\":{\"goals\":[1,3,17]},\"skills\":{\"1\":\"b\",\"2\":\"b\",\"3\":\"b\",\"4\":\"e\",\"5\":\"e\"},\"phone\":\"94379347\"}",
            "Status": "active",
            "Description": "Tell us about yourself",
            "PK": "usr-neaven77@gmail.com",
            "UserName": "Yong Kheng Seo",
            "GitLabInfo": "[\n    {\n        \"id\": 6499199,\n        \"name\": \"Neaven Seo\",\n        \"username\": \"neaven77\",\n        \"state\": \"active\",\n        \"avatar_url\": \"https://secure.gravatar.com/avatar/d611aa060da4150baf726d83bbf77f02?s=80&d=identicon\",\n        \"web_url\": \"https://gitlab.com/neaven77\"\n    }\n]",
            "EntityType": "user",
            "Tenent": "p4gTENANT",
            "Name": "Yong Kheng Seo"
          }
        ],
        "Count": 2,
        "ScannedCount": 2
      }

      //*///

      if (response && response.Items) {
        let projects: {
          name: string
        }[] = [];

        for (let i = 0; i < response.Items.length; i++) {
          const item = response.Items[i];
          if (item.EntityType === 'user') {

            payload.pk = item.PK;
            payload.sk = item.SK;
            payload.username = item.UserName;
            payload.email = item.UserEmail;
            payload.name = item.Name;
            payload.description = item.Description;

            if (item.CustomInfo) {
              const custominfo = JSON.parse(item.CustomInfo);
              payload.availability = custominfo.availability;
              payload.interests = custominfo.interests;
              payload.skillset = custominfo.skills;
              payload.phone = custominfo.phone;
            }

            if (item.GitLabInfo) {
              const currentGitLabInfo = JSON.parse(item.GitLabInfo)[0];
              if (currentGitLabInfo) {
                payload.gitLabUsername = currentGitLabInfo.username;
                payload.gitLabId = currentGitLabInfo.id;
              }
            }

          } else if (item.EntityType === "project-member") {
            projects.push({ name: item.ProjectName });

          }
        }

        payload.projects = projects;
        this.storage.save(CURRENT_USER, payload);

      }

    } catch (e) {
      return Promise.reject(e.message);
    }

    return payload;

  }

  public async getAllUsers(): Promise<User[]> {
    // https://puwyqb4m0a.execute-api.us-east-1.amazonaws.com/dev/users
    this.endpoint = 'users';
    const payload: User[] = [];

    try {
      const response =
        await this.get<{
          Items: {
            UserEmail: string,
            SName: string,
            SK: string,
            CustomInfo: string,
            Status: string,
            Description: string,
            PK: string,
            UserName: string,
            GitLabInfo: string,
            EntityType: string,
            Tenent: string,
            Name: string
          }[],
          Count: number,
          ScannedCount: number
        }>();
      // console.log("getAllUsers(): " + JSON.stringify(response, null, 2));

      // sample payload
      /*
      let response = {
        "Items": [
          {
            "UserEmail": "joshy.paul.c@gmail.com",
            "SName": "joshy paul",
            "SK": "usr-joshy.paul.c@gmail.com",
            "CustomInfo": "{\"availability\":{\"weekendEvening\":true,\"weekendAfternoon\":true,\"weekendMorning\":\"\",\"weekdayEvening\":\"\",\"weekdayAfternoon\":\"\",\"weekdayMorning\":\"\"},\"interests\":{\"economic\":true,\"education\":true,\"energy\":true,\"health\":\"\",\"hunger\":\"\",\"inequality\":\"\",\"poverty\":\"\"},\"skills\":{\"marketing\":\"Experienced\",\"dataAnalysis\":\"Experienced\"},\"phone\":\"\"}",
            "Status": "active",
            "Description": "I am great",
            "PK": "usr-joshy.paul.c@gmail.com",
            "UserName": "Joshy Paul",
            "GitLabInfo": "[{\"id\":6523408,\"name\":\"Joshy Paul\",\"username\":\"joshy.paul.c\",\"state\":\"active\",\"avatar_url\":\"https://secure.gravatar.com/avatar/0154020855247b987ae90ce379ecd372?s=80&d=identicon\",\"web_url\":\"https://gitlab.com/joshy.paul.c\"}]",
            "EntityType": "user",
            "Tenent": "p4gTENANT",
            "Name": "Joshy Paul"
          },
          {
            "UserEmail": "neaven77@gmail.com",
            "SName": "yong kheng seo",
            "SK": "usr-neaven77@gmail.com",
            "CustomInfo": "{\"availability\":{\"weekendEvening\":true,\"weekendAfternoon\":\"\",\"weekendMorning\":true,\"weekdayEvening\":\"\",\"weekdayAfternoon\":\"\",\"weekdayMorning\":\"\"},\"interests\":{\"goal1\":true,\"goal2\":\"\",\"goal3\":\"\",\"goal4\":\"\",\"goal5\":\"\",\"goal6\":\"\",\"goal7\":\"\",\"goal8\":\"\",\"goal9\":\"\",\"goal10\":\"\",\"goal11\":\"\",\"goal12\":\"\",\"goal13\":\"\",\"goal14\":\"\",\"goal15\":\"\",\"goal16\":\"\",\"goal17\":\"\"},\"skills\":{\"marketing\":\"Beginner\",\"dataAnalysis\":\"Beginner\"},\"phone\":\"94379347\"}",
            "Status": "active",
            "Description": "Tell us about yourself",
            "PK": "usr-neaven77@gmail.com",
            "UserName": "Yong Kheng Seo",
            "GitLabInfo": "[{\"id\": 6499199,\"name\":\"Neaven Seo\",\"username\":\"neaven77\",\"state\":\"active\",\"avatar_url\":\"https://secure.gravatar.com/avatar/d611aa060da4150baf726d83bbf77f02?s=80&d=identicon\",\"web_url\":\"https://gitlab.com/neaven77\"}]",
            "EntityType": "user",
            "Tenent": "p4gTENANT",
            "Name": "Yong Kheng Seo"
          }
        ],
        "Count": 2,
        "ScannedCount": 2
      }
      //*///

      if (response && response.Items) {
        for (let i = 0; i < response.Items.length; i++) {
          const item = response.Items[i];
          const user: User = {
            pk: item.SK,
            sk: item.SK,
            username: item.UserName,
            email: item.UserEmail,
            name: item.Name,
            description: item.Description,
            tenantId: item.Tenent
          }

          if (item.CustomInfo) {
            const custominfo = JSON.parse(item.CustomInfo);
            user.availability = custominfo.availability;
            user.interests = custominfo.interests;
            user.skillset = custominfo.skills;
            user.phone = custominfo.phone;
          }

          if (item.GitLabInfo) {
            const currentGitLabInfo = JSON.parse(item.GitLabInfo)[0];
            if (currentGitLabInfo) {
              user.gitLabUsername = currentGitLabInfo.username;
            }
          }

          payload.push(user);
        }
      }

    } catch (e) {
      return Promise.reject(e.message);
    }

    return payload;
  }

  //#endregion
}
